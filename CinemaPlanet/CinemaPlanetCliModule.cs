﻿using CinemaPlanet.Business;
using CinemaPlanet.Cli.Helpers;
using Ninject;
using Ninject.Modules;

namespace CinemaPlanet.Cli
{
    public class CinemaPlanetCliModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<IMenu>().To<Menu>();
            Kernel.Bind<IIoHelper>().To<IoHelper>();
            Kernel.Bind<IConsoleWriter>().To<ConsoleWriter>();
            Kernel.Load(new CinemaPlanetBusinessModule());
        }
    }
}
