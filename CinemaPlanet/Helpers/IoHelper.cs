﻿using CinemaPlanet.Business.Models;
using CinemaPlanet.Business.Services;
using System;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;

namespace CinemaPlanet.Cli.Helpers
{
    public interface IIoHelper
    {
        string GetPostcode();
        string GetEmail();
        Genre GetGenre();
        int GetIntFromUser(string message);
        string GetPhoneNumber();
        DateTime GetScreeningDayFromUser();
        DateTime GetHourFromUser(string message);
        string GetStringFromUser(string message);
        string GetWebsite();
        string GetWeek(DateTime programmeBlStartDay);
        string NumberToLetter(int number);
    }

    public class IoHelper : RegexProvider, IIoHelper
    {
        private readonly ICinemasService _cinemasService;
        private readonly IProgrammesService _programmesService;
        private readonly IFilmsService _filmsService;

        public IoHelper(ICinemasService cinemasService, IProgrammesService programmesService, IFilmsService filmsService)
        {
            _cinemasService = cinemasService;
            _programmesService = programmesService;
            _filmsService = filmsService;
        }

        public string GetStringFromUser(string message)
        {
            string result = "";

            do
            {
                Console.WriteLine(message);
                result = Console.ReadLine();
            } while (String.IsNullOrWhiteSpace(result));

            return result;
        }

        public int GetIntFromUser(string message)
        {
            int result;

            while (!int.TryParse(GetStringFromUser(message), out result))
            {
                Console.WriteLine("Wrong input. Try again...");
            }

            return result;
        }

        public Genre GetGenre()
        {
            Genre result;

            while (!Enum.TryParse(GetStringFromUser(PrintGenres()), out result) || (!Enum.IsDefined(typeof(Genre), result)))
            {
                Console.WriteLine("Wrong input");
            }

            return result;
        }

        private string PrintGenres()
        {
            string result = "";

            for (int i = 1; i < Enum.GetNames(typeof(Genre)).Count() + 1; i++)
            {
                if (i != Enum.GetNames(typeof(Genre)).Count())
                {
                    result += $"{i}: {Enum.GetName(typeof(Genre), i)}\n";
                }
                else
                {
                    result += $"{i}: {Enum.GetName(typeof(Genre), i)}";
                }
            }
            return result;
        }

        public string GetWebsite()
        {
            GetUrlFromUser(out Uri uri, out bool urlExist);

            while (uri.Host != "www.filmweb.pl" || uri.LocalPath.Equals("/") || urlExist == false)
            {
                Console.WriteLine("Wrong url");
                GetUrlFromUser(out uri, out urlExist);
            }

            return uri.ToString();
        }

        private void GetUrlFromUser(out Uri uri, out bool urlExist)
        {
            string url = GetStringFromUser("Type film's FilmWeb Page");
            ValidHttpUrl(url, out uri);
            urlExist = CheckIfUrlExists(uri);
        }

        public static bool ValidHttpUrl(string url, out Uri uri)
        {
            if (!url.Contains("http"))
            {
                url = "http://" + url;
            }

            if (Uri.TryCreate(url, UriKind.Absolute, out uri))
                return (uri.Scheme == Uri.UriSchemeHttp ||
                        uri.Scheme == Uri.UriSchemeHttps);

            return false;
        }

        public static bool CheckIfUrlExists(Uri url)
        {
            try
            {
                WebRequest req = WebRequest.Create(url);
                WebResponse res = req.GetResponse();
                return true;
            }
            catch (WebException ex)
            {
                Console.WriteLine(ex.Message);
                if (ex.Message.Contains("remote name could not be resolved"))
                {
                    Console.WriteLine("Url is Invalid");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return false;
        }

        public string GetEmail()
        {
            var input = GetStringFromUser("Enter e-mail");
            var match = RegexEmail.Match(input);

            while (!match.Success)
            {
                input = GetStringFromUser("Enter e-mail");
                match = RegexEmail.Match(input);

                if (!match.Success)
                {
                    Console.WriteLine($"{input} is not an email");
                    continue;
                }
            }

            return match.Value;
        }

        public string GetPhoneNumber()
        {
            var input = GetStringFromUser("Enter phone number");
            var matchLandline = RegexLandlinePhone.Match(input);
            var matchMobile = RegexMobilePhone.Match(input);

            while (!matchLandline.Success && !matchMobile.Success)
            {
                input = GetStringFromUser("Enter phone number");
                matchLandline = RegexLandlinePhone.Match(input);
                matchMobile = RegexMobilePhone.Match(input);

                if (!matchLandline.Success || !matchMobile.Success)
                {
                    Console.WriteLine($"{input} is not valid phone number");
                    continue;
                }
            }

            if (matchLandline.Success)
            {
                return SetLandlineNumber(matchLandline);
            }

            if (matchMobile.Success)
            {
                return SetMobileNumber(matchMobile);
            }

            throw new Exception("Wrong input");
        }

        private static string SetMobileNumber(Match matchMobile)
        {
            return $"+{matchMobile.Groups["internationalPrefix"].Value} " +
                                $"{matchMobile.Groups["firstThree"].Value}" +
                                $"{matchMobile.Groups["secondThree"].Value}" +
                                $"{matchMobile.Groups["thirdThree"].Value}";
        }

        private static string SetLandlineNumber(Match matchLandline)
        {
            return $"{matchLandline.Groups["internationalPrefix"].Value} " +
                                $"{matchLandline.Groups["nationalAreaCode"].Value} " +
                                $"{matchLandline.Groups["firstThree"].Value}" +
                                $"{matchLandline.Groups["secondTwo"].Value}" +
                                $"{matchLandline.Groups["thirdTwo"].Value} ";
        }

        public string GetPostcode()
        {
            var input = GetStringFromUser("Type postcode");
            var match = RegexPostcode.Match(input);

            while (!match.Success)
            {
                input = GetStringFromUser("Enter postcode");
                match = RegexPostcode.Match(input);

                if (!match.Success)
                {
                    Console.WriteLine($"{input} is not a valid input");
                    continue;
                }
            }

            return match.Value;
        }

        public DateTime GetScreeningDayFromUser()
        {
            string dateString;
            DateTime result = DateTime.MinValue;
            CultureInfo provider = CultureInfo.InvariantCulture;

            while (result == DateTime.MinValue)
            {
                dateString = GetStringFromUser("Type day of the screening");
                result = GetDateTime(dateString, result, provider);
            }

            return result;
        }

        private static DateTime GetDateTime(string dateString, DateTime result, CultureInfo provider)
        {
            try
            {
                result = DateTime.Parse(dateString, provider);
            }
            catch (FormatException)
            {
                Console.WriteLine($"{dateString} is not in the correct format.");
            }

            return result;
        }

        public DateTime GetHourFromUser(string message)
        {
            var input = GetStringFromUser(message);
            DateTime result = DateTime.MinValue;

            while (result == DateTime.MinValue)
            {
                result = GetTime(input, result);
            }

            return result;
        }

        private DateTime GetTime(string dateString, DateTime result)
        {
            try
            {
                result = DateTime.Parse(dateString, CultureInfo.InvariantCulture);
            }
            catch (FormatException)
            {
                Console.WriteLine($"{dateString} is not in the correct format.");
            }

            return result;
        }

        public string GetWeek(DateTime startDay)
        {
            return $"{startDay:dd/MM/yyyy}-{startDay.AddDays(6):dd/MM/yyyy}";
        }

        public string NumberToLetter(int number)
        {
            const string letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            if (number > letters.Length)
            {
                throw new ArgumentOutOfRangeException();
            }

            return letters.ElementAt(number).ToString();
        }
    }
}
