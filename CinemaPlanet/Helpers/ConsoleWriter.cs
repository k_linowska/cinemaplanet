﻿using CinemaPlanet.Business.Models;
using CinemaPlanet.Business.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CinemaPlanet.Cli.Helpers
{
    public interface IConsoleWriter
    {
        void PrintAuditoria(List<AuditoriumBl> auditoria);
        void PrintCinemas(List<CinemaBl> cinemas);
        void PrintFilms(List<FilmBl> films);
        void PrintProgramme(ProgrammeBl programme);
        void PrintTickets(List<TicketDetails> tickets);
    }

    public class ConsoleWriter : IConsoleWriter
    {
        private readonly IIoHelper _ioHelper;
        private readonly ICinemasService _cinemasService;
        private readonly IProgrammesService _programmesService;
        private readonly IFilmsService _filmsService;

        public ConsoleWriter(IIoHelper ioHelper, ICinemasService cinemasService, IProgrammesService programmesService, IFilmsService filmsService)
        {
            _ioHelper = ioHelper;
            _cinemasService = cinemasService;
            _programmesService = programmesService;
            _filmsService = filmsService;
        }

        public void PrintAuditoria(List<AuditoriumBl> auditoria)
        {
            foreach (var item in auditoria)
            {
                Console.WriteLine($"Auditorium {item.AuditoriumNumber}: number of rows - {item.Rows}, number of seat in a row - {item.SeatsInRow}");
            }
        }

        public void PrintCinemas(List<CinemaBl> cinemas)
        {
            for (int i = 0; i < cinemas.Count; i++)
            {
                Console.WriteLine(i + 1 + ": " + cinemas.ElementAt(i).Name);
            }
        }

        public void PrintFilms(List<FilmBl> films)
        {
            for (int i = 0; i < films.Count; i++)
            {
                Console.WriteLine(i + 1 + ": " + films.ElementAt(i).Title);
            }
        }

        public void PrintProgramme(ProgrammeBl programme)
        {
            Console.WriteLine($"Screenings in cinema {_cinemasService.GetCinemaBlAsync(programme.CinemaId).Result.Name} on {_ioHelper.GetWeek(programme.StartDay)}");
            PrintFilmsScreenings(programme);
        }

        private void PrintFilmsScreenings(ProgrammeBl programme)
        {
            foreach (var filmScreeningId in programme.FilmScreeningsId)
            {
                var filmScreening = _programmesService.GetFilmScreeningsBlAsync(filmScreeningId).Result;
                var film = _filmsService.GetFilmBlAsync(filmScreening.FilmId).Result;
                PrintFilmInfo(film);
                PrintScreeningInfo(filmScreening);
            }
        }

        private static void PrintFilmInfo(FilmBl film)
        {
            Console.WriteLine($"Screenings for {film.Title}\n" +
                                $"Genre: {film.FilmGenre}\n" +
                                $"Summary: {film.Summary}\n" +
                                $"Running time {film.RunningTime} minutes");
        }

        private void PrintScreeningInfo(FilmScreeningBl filmScreening)
        {
            foreach (var screeningId in filmScreening.ScreeningsId)
            {
                var screening = _programmesService.GetScreeningBlAsync(screeningId).Result;
                Console.WriteLine($"Id: {screening.Id}. {screening.TimeOfScreening.ToShortTimeString()} in auditorium No. {_cinemasService.GetAuditoriumBlAsync(screening.AuditoriumId).Result.AuditoriumNumber}");
            }
        }

        public void PrintTickets(List<TicketDetails> tickets)
        {
            if (tickets.Count == 0)
            {
                Console.WriteLine("List of tickets is empty");
            }
            else
            {
                foreach (var ticket in tickets)
                {
                    PrintTicket(ticket);
                }
            }
        }

        private void PrintTicket(TicketDetails ticket)
        {
            Console.WriteLine($"Film title: {ticket.FilmTitle}\n" +
                              $"Screening date: {ticket.ScreeningDateTime}\n" +
                              $"Cinema: {ticket.CinemaName}, address: {ticket.CinemaAddress}\n" +
                              $"Seat: {ticket.ClientSeat}\n");
        }
    }
}
