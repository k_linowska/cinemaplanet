﻿using System.Text.RegularExpressions;

namespace CinemaPlanet.Cli.Helpers
{
    public abstract class RegexProvider
    {
        public Regex RegexEmail = new Regex(
            @"^(?<username>[\w\.-]{2,})@(?<domain>[\dA-z]{2,})\.((?<postfix>[\dA-z]{3})\.)?(?<country>[\dA-z]{2,3})$");

        public Regex RegexLandlinePhone = new Regex(
            @"^\(?(?<internationalPrefix>([0]{2}|\+)?[0-9]{2})?[ ]?(?<nationalAreaCode>\(?[0-9]{2}\)?)[ ]?(?<firstThree>[0-9]{3})\)?[-. ]?(?<secondTwo>[0-9]{2})[-. ]?(?<thirdTwo>[0-9]{2})$");

        public Regex RegexMobilePhone = new Regex(
            @"^\(?\+?(?<internationalPrefix>[0-9]{2})?[ ]?\)?(?<firstThree>\+?[0-9]{3})?[ ]?(?<secondThree>[0-9]{3})\)?[-. ]?(?<thirdThree>[0-9]{3})$");

        public Regex RegexPostcode = new Regex("^(?<groupOne>[0-9]{2})-(?<groupTwo>[0-9]{3})$");
    }
}