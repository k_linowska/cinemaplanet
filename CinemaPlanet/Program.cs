﻿using CinemaPlanet.Business.Models;
using CinemaPlanet.Business.Services;
using CinemaPlanet.Cli.Helpers;
using Ninject;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using CinemaPlanet.Business.Helpers;

namespace CinemaPlanet.Cli
{
    internal class Program
    {
        private readonly IMenu _menu;
        private readonly IConsoleWriter _consoleWriter;
        private readonly IIoHelper _ioHelper;
        private readonly ICinemasService _cinemasService;
        private readonly IFilmsService _filmsService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IProgrammesService _programmesService;
        private readonly IClientsService _clientsService;
        private readonly ITicketsService _ticketsService;
        private readonly IPurchaseService _purchaseService;
        private readonly IProgrammeValidator _programmeValidator;

        public Program(
            IMenu menu,
            IIoHelper ioHelper,
            IConsoleWriter consoleWriter,
            ICinemasService cinemasService,
            IFilmsService filmsService,
            IDateTimeHelper dateTimeHelper,
            IProgrammesService programmesService,
            IClientsService clientsService,
            ITicketsService ticketsService,
            IPurchaseService purchaseService,
            IProgrammeValidator programmeValidator)
        {
            _menu = menu;
            _ioHelper = ioHelper;
            _consoleWriter = consoleWriter;
            _cinemasService = cinemasService;
            _filmsService = filmsService;
            _dateTimeHelper = dateTimeHelper;
            _programmesService = programmesService;
            _clientsService = clientsService;
            _ticketsService = ticketsService;
            _purchaseService = purchaseService;
            _programmeValidator = programmeValidator;
        }

        static void Main(string[] args)
        {
            IKernel kernel = new StandardKernel(new CinemaPlanetCliModule());
            var program = kernel.Get<Program>();
            program.SetCommands();
            program.ProgramLoop();
        }

        private void SetCommands()
        {
            _menu.SetCommand(AddFilm, "Add Film");
            _menu.SetCommand(AddCinema, "Add Cinema");
            _menu.SetCommand(AddProgramme, "Add Programme");
            _menu.SetCommand(PrintProgramme, "Print Programme");
            _menu.SetCommand(RegisterClient, "Register Client");
            _menu.SetCommand(PurchaseTickets, "Purchase Tickets");
            _menu.SetCommand(PrintClientsTickets, "Print Tickets");
            _menu.SetCommand(Exit, "Exit");
        }

        public void ProgramLoop()
        {
            do
            {
                _menu.PrintAllCommands();
                _menu.Execute(Console.ReadLine());
            }
            while (true);
        }

        private void AddFilm()
        {
            var film = new FilmBl
            {
                Title = _ioHelper.GetStringFromUser("Type film's title"),
                FilmGenre = _ioHelper.GetGenre(),
                RunningTime = _ioHelper.GetIntFromUser("Type film's running time in minutes"),
                Summary = _ioHelper.GetStringFromUser("Type film's summary"),
                Website = _ioHelper.GetWebsite()
            };

            _filmsService.AddFilmAsync(film).Wait();
        }

        private void AddCinema()
        {
            var cinemaBl = new CinemaBlToAdd()
            {
                Name = _ioHelper.GetStringFromUser("Type cinema's name"),
                StreetName = _ioHelper.GetStringFromUser("Type street name"),
                StreetNumber = _ioHelper.GetStringFromUser("Type street number"),
                OpeningHour = _ioHelper.GetHourFromUser("Type opening hour"),
                City = _ioHelper.GetStringFromUser("Type city"),
                Postcode = _ioHelper.GetPostcode(),
                Email = _ioHelper.GetEmail(),
                PhoneNumber = _ioHelper.GetPhoneNumber(),
                Auditoria = AddAuditoria()
            };

            _cinemasService.AddCinemaAsync(cinemaBl).Wait();
        }

        private List<AuditoriumBl> AddAuditoria()
        {
            string userAnswer = "y";
            List<AuditoriumBl> auditoria = new List<AuditoriumBl>();
            int auditoriumNumber = 0;
            Console.WriteLine("Adding auditoria");

            while (userAnswer == "y")
            {
                ++auditoriumNumber;

                var auditoriumBl = new AuditoriumBl
                {
                    AuditoriumNumber = auditoriumNumber,
                    Rows = SetRows(auditoriumNumber),
                    SeatsInRow = _ioHelper.GetIntFromUser("Type number of seats in a row"),
                };

                auditoria.Add(auditoriumBl);
                userAnswer = _ioHelper.GetStringFromUser("Do you want to add another auditorium? (y/n)").ToLower();
            }

            return auditoria;
        }

        private int SetRows(int auditoriumNumber)
        {
            string message = $"Type number of rows (between 1 and 26) for auditorium {auditoriumNumber}";
            int numberOfRows = _ioHelper.GetIntFromUser(message);

            while (numberOfRows <= 0 || numberOfRows > 26)
            {
                Console.WriteLine("Number of rows must be between 1 and 26");
                numberOfRows =
                    _ioHelper.GetIntFromUser(message);
            }

            return numberOfRows;
        }

        private void AddProgramme()
        {
            try
            {
                string week;
                var cinema = GetCinema("Available cinemas: ", _cinemasService.GetAllCinemasAsync().Result);
                var film = GetFilmToSetScreenings("Available films: ", _filmsService.GetAllFilmsAsync().Result);
                var programmeBl = _programmesService.FindProgrammeAsync(cinema.Id, film.Id).Result;
                ProgrammeBl programmeToSet = null;

                week = programmeBl == null ? GetProgrammesWeek(_ioHelper.GetScreeningDayFromUser()) : _dateTimeHelper.FindNextWeek(programmeBl.StartDay);

                Console.WriteLine($"Adding film {film.Title} for cinema {cinema.Name} for {week}");
                programmeToSet = GetProgramme(week, cinema.Id);
                SetFilmScreenings(programmeToSet, film);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private CinemaBl GetCinema(string message, List<CinemaBl> cinemas)
        {
            Console.WriteLine(message);
            _consoleWriter.PrintCinemas(cinemas);
            int chosenCinema = _ioHelper.GetIntFromUser("Enter cinema's number: ");
            return cinemas.ElementAt(chosenCinema - 1);
        }

        private FilmBl GetFilmToSetScreenings(string message, List<FilmBl> films)
        {
            Console.WriteLine(message);
            _consoleWriter.PrintFilms(films);
            return films.ElementAt(_ioHelper.GetIntFromUser("Enter film's number: ") - 1);
        }

        private ProgrammeBl GetProgramme(string week, int cinemaId)
        {
            var programmeBl = _programmesService.FindProgrammeAsync(week, cinemaId).Result;

            if (programmeBl == null)
            {
                programmeBl = new ProgrammeBl
                {
                    CinemaId = cinemaId,
                    StartDay = DateTime.ParseExact(week.Substring(0, week.IndexOf("-")), "dd/MM/yyyy", CultureInfo.InvariantCulture)
                };

                _programmesService.AddProgrammeAsync(programmeBl).Wait();
            }

            return programmeBl;
        }

        private void SetFilmScreenings(ProgrammeBl programmeBl, FilmBl film)
        {
            string anotherFilmScreeningsUserAnswer = "y";

            while (anotherFilmScreeningsUserAnswer == "y")
            {
                programmeBl.FilmScreeningsId.Add(AddFilmScreenings(programmeBl, film));
                anotherFilmScreeningsUserAnswer = _ioHelper.GetStringFromUser
                    ($"Do you want to add another film screening in {_cinemasService.GetCinemaBlAsync(programmeBl.CinemaId).Result.Name} for {_ioHelper.GetWeek(programmeBl.StartDay)} (y/n)").ToLower();

                if (anotherFilmScreeningsUserAnswer == "y")
                {
                    film = GetFilmToSetScreenings("Available films: ", _filmsService.GetAllFilmsAsync().Result);
                }
            }
        }

        private int AddFilmScreenings(ProgrammeBl programmeBl, FilmBl film)
        {
            var filmScreenings = _programmesService.FindFilmScreeningAsync(programmeBl.Id, film.Id).Result ?? new FilmScreeningBl { FilmId = film.Id };
            string anotherScreeningUserAnswer = "y";

            while (anotherScreeningUserAnswer == "y")
            {
                filmScreenings.ScreeningsId.Add(GetScreeningTime(programmeBl, film.Id).Id);
                _programmesService.AddFilmScreeningAsync(filmScreenings).Wait();
                programmeBl.FilmScreeningsId.Add(filmScreenings.Id);
                _programmesService.UpdateProgrammeAsync(programmeBl).Wait();
                anotherScreeningUserAnswer = _ioHelper.GetStringFromUser
                    ($"Do you want to add another screening for {film.Title} " +
                    $"in {_cinemasService.GetCinemaBlAsync(programmeBl.CinemaId).Result.Name} for {_ioHelper.GetWeek(programmeBl.StartDay)} (y/n)").ToLower();
            }

            return filmScreenings.Id;
        }

        private ScreeningBl GetScreeningTime(ProgrammeBl programmeBl, int filmId)
        {
            ScreeningBl screening = new ScreeningBl();

            while (screening.Id == 0)
            {
                try
                {
                    DateTime timeOfScreening = _ioHelper.GetHourFromUser("Enter screening time");
                    var auditorium = GetAuditorium(programmeBl.CinemaId);
                    if (_programmeValidator.IsTimeAndAuditoriumAvailableAsync(timeOfScreening, auditorium.AuditoriumNumber, programmeBl.Id, filmId).Result)
                    {
                        screening.AuditoriumId = auditorium.Id;
                        screening.TimeOfScreening = timeOfScreening;
                        _programmesService.AddScreeningAsync(screening).Wait();
                    }
                    else
                    {
                        Console.WriteLine($"{timeOfScreening:HH:mm} for auditorium {auditorium.AuditoriumNumber} not available");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            return screening;
        }

        private AuditoriumBl GetAuditorium(int cinemaId)
        {
            List<AuditoriumBl> auditoria = _cinemasService.GetCinemasAuditoriaAsync(cinemaId).Result;
            Console.WriteLine("Available auditoria: ");
            _consoleWriter.PrintAuditoria(auditoria);
            int chosenAuditorium = _ioHelper.GetIntFromUser("Enter auditorium's number: ");
            return auditoria.ElementAt(chosenAuditorium - 1);
        }

        private string GetProgrammesWeek(DateTime result)
        {
            while (result.DayOfWeek != DayOfWeek.Friday)
            {
                result = result.AddDays(-1);
            }

            return $"{result:dd/MM/yyyy}-{result.AddDays(6):dd/MM/yyyy}";
        }

        private void PrintProgramme()
        {
            var cinema = GetCinema("Choose cinema", _cinemasService.GetAllCinemasAsync().Result);
            string week = GetProgrammesWeek(DateTime.Now);
            Console.WriteLine($"Programme for {cinema.Name} for {week}");

            if (_ioHelper.GetStringFromUser("Do you want to change the date? (y/n)").ToLower() == "y")
            {
                week = GetProgrammesWeek(_ioHelper.GetScreeningDayFromUser());
            }

            try
            {
                var programme = _programmesService.FindProgrammeAsync(week, cinema.Id).Result;
                _consoleWriter.PrintProgramme(programme);
                ExportToJsonOrXml(programme);
            }
            catch (NullReferenceException e)
            {
                Console.WriteLine($"No screenings on week {week}");
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private void ExportToJsonOrXml(ProgrammeBl programme)
        {
            if (_ioHelper.GetStringFromUser("Do you want to export programme to JSON or XML file? (y/n)").ToLower() == "y")
            {
                var format = _ioHelper.GetStringFromUser("Type format JSON or XML").ToLower();

                while (format != "json" && format != "xml")
                {
                    Console.WriteLine("Invalid format");
                    format = _ioHelper.GetStringFromUser("Type format JSON or XML").ToLower();
                }

                _programmesService.SerializeProgrammeAsync(format, _ioHelper.GetStringFromUser("Type file path"), programme.Id).Wait();
            }
        }

        private void RegisterClient()
        {
            var client = new ClientBl
            {
                FirstName = _ioHelper.GetStringFromUser("Type your first name"),
                LastName = _ioHelper.GetStringFromUser("Type your last name"),
                Email = SetClientsEmail(_ioHelper.GetEmail())
            };

            _clientsService.AddCLient(client);
        }

        private string SetClientsEmail(string email)
        {
            while (_clientsService.IsEmailTaken(email).Result)
            {
                email = _ioHelper.GetEmail();
            }

            return email;
        }

        private void PurchaseTickets()
        {
            var email = _ioHelper.GetEmail();
            var client = _clientsService.GetCLient(email).Result;

            if (client == null)
            {
                Console.WriteLine($"Client with email {email} is not registered.");
                return;
            }

            var dayOfScreening = SetScreeningDay(_ioHelper.GetScreeningDayFromUser());
            var programmes = _programmesService.FindListOfProgrammesAsync(GetProgrammesWeek(dayOfScreening)).Result;
            var screening = ChooseScreening(programmes);
            var programme = _programmesService.FindProgrammeAsync(screening.Id).Result;
            var dateTimeOfScreening = _dateTimeHelper.CombineDates(dayOfScreening, screening.TimeOfScreening);

            var purchase = new PurchaseDetails
            {
                ClientId = client.Id,
                Client = client,
                ScreeningId = screening.Id,
                DateTimeOfScreening = dateTimeOfScreening,
                Seats = ChooseSeats(_cinemasService.GetAuditoriumBlAsync(screening.AuditoriumId).Result, screening.Id, dateTimeOfScreening),
                Cinema = programme.Cinema,
                Film = _filmsService.FindFilmAsync(screening.Id).Result
            };

            purchase.Price = _purchaseService.SetPriceAsync(purchase.ClientId, purchase.Seats.Count, dateTimeOfScreening).Result;
            _ticketsService.AddTicketAsync(purchase).Wait();
            _purchaseService.SendReceipt(purchase);
        }

        private DateTime SetScreeningDay(DateTime dayOfScreening)
        {
            while (dayOfScreening < DateTime.Now || !_programmesService.DoesProgrammeExistAsync(GetProgrammesWeek(dayOfScreening)).Result)
            {
                Console.WriteLine($"Screening day not available");
                dayOfScreening = _ioHelper.GetScreeningDayFromUser();
            }

            return dayOfScreening;
        }

        protected ScreeningBl ChooseScreening(List<ProgrammeBl> programmes)
        {
            foreach (var programme in programmes)
            {
                _consoleWriter.PrintProgramme(programme);
            }

            var listOfScreeningsId = new List<int>();

            foreach (var screeningIdToChoose in programmes.SelectMany(p => p.FilmScreenings.SelectMany(f => f.ScreeningsId)))
            {
                listOfScreeningsId.Add(screeningIdToChoose);
            }

            var screeningId = _ioHelper.GetIntFromUser("Type screenings id");
            var screening = _programmesService.GetScreeningBlAsync(screeningId).Result;

            while (screening == null || !listOfScreeningsId.Contains(screeningId))
            {
                Console.WriteLine("Wrong screening");
                screeningId = _ioHelper.GetIntFromUser("Type screenings id");
                screening = _programmesService.GetScreeningBlAsync(screeningId).Result;
            }

            return screening;
        }

        private List<string> ChooseSeats(AuditoriumBl auditorium, int screeningId, DateTime dateTimeOfScreening)
        {
            var chosenSeats = new List<string>();
            var userAnswer = "y";

            while (userAnswer == "y")
            {
                var row = SetRow(auditorium);
                var seat = SetSeat(auditorium);
                var chosenSeat = row + seat;

                if (!_purchaseService.IsSeatAvailableAsync(chosenSeat, chosenSeats, screeningId, dateTimeOfScreening).Result)
                {
                    Console.WriteLine($"Seat {chosenSeat} is already taken");
                    continue;
                }

                chosenSeats.Add(chosenSeat);
                userAnswer = _ioHelper.GetStringFromUser("Do you want to choose another seat?(y/n)").ToLower();
            }

            return chosenSeats;
        }

        private string SetRow(AuditoriumBl auditorium)
        {
            var availableRows = new List<string>();

            for (int i = 0; i < auditorium.Rows; i++)
            {
                availableRows.Add(_ioHelper.NumberToLetter(i));
            }

            var row = _ioHelper
                .GetStringFromUser(
                    $"Pick a row from A to {_ioHelper.NumberToLetter(auditorium.Rows - 1)}").ToUpper();

            while (!availableRows.Contains(row))
            {
                Console.WriteLine($"Row {row} does not exist");
                row = _ioHelper
                    .GetStringFromUser(
                        $"Pick a row from A to {_ioHelper.NumberToLetter(auditorium.Rows - 1)}").ToUpper();
            }

            return row;
        }

        private int SetSeat(AuditoriumBl auditorium)
        {
            var seat = _ioHelper
                .GetIntFromUser(
                    $"Pick a seat from 1 to {auditorium.SeatsInRow}");

            while (seat <= 0 || seat > auditorium.SeatsInRow)
            {
                Console.WriteLine($"Seat {seat} does not exist");
                seat = _ioHelper
                    .GetIntFromUser(
                        $"Pick a seat from 1 to {auditorium.SeatsInRow}");
            }

            return seat;
        }

        private void PrintClientsTickets()
        {
            var clientId = _ioHelper.GetIntFromUser("Type client's id");
            var client = _clientsService.GetCLient(clientId).Result;
            if (client == null)
            {
                Console.WriteLine($"Client with id {clientId} does not exist");
                return;
            }

            _consoleWriter.PrintTickets(client.Tickets);
        }

        private void Exit()
        {
            Environment.Exit(0);
        }
    }
}
