﻿using System;
using System.Globalization;
using System.Linq;
using AutoMapper;
using CinemaPlanet.Business.Models;
using CinemaPlanet.Data.Models;
using Ninject;
using Ninject.Modules;

namespace CinemaPlanet.Business.Helpers
{
    public class AutoMapperModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IMapper>().ToMethod(AutoMapper).InSingletonScope();
        }

        private IMapper AutoMapper(Ninject.Activation.IContext context)
        {
            Mapper.Initialize(cfg =>
            {
                cfg.ConstructServicesUsing(type => context.Kernel.Get(type));

                cfg.CreateMap<Film, FilmBl>();
                cfg.CreateMap<FilmBl, Film>();
                cfg.CreateMap<Client, ClientBl>()
                    .ForMember(cbl => cbl.TicketsId, m => m.MapFrom(c => c.Tickets.Select(t => t.Id))); ;
                cfg.CreateMap<ClientBl, Client>()
                    .ForMember(c => c.Tickets, opt => opt.Ignore());
                cfg.CreateMap<ClientRegistration, Client>()
                    .ForMember(c => c.Tickets, opt => opt.Ignore())
                    .ForMember(c => c.Id, opt => opt.Ignore());
                cfg.CreateMap<Cinema, CinemaBl>()
                    .ForMember(cbl => cbl.AuditoriaId, m => m.MapFrom(c => c.Auditoria.Select(i => i.Id)))
                    .ForMember(cbl => cbl.OpeningHour,
                        opts => opts.MapFrom(
                            src => DateTime.Parse(src.OpeningHour)));
                cfg.CreateMap<Cinema, CinemaGeneralInfo>()
                    .ForMember(cbl => cbl.City, m => m.MapFrom(c => c.Address.Substring(c.Address.LastIndexOf(' '))))
                    .ForMember(cbl => cbl.Address, opts => opts.MapFrom(c => c.Address.Substring(0, c.Address.LastIndexOf(' '))));
                cfg.CreateMap<CinemaBlToAdd, Cinema>()
                    .ForMember(c => c.Auditoria, opt => opt.Ignore())
                    .ForMember(c => c.Address,
                        opts => opts.MapFrom(
                            src => string.Format($"{src.StreetName} {src.StreetNumber}, {src.Postcode} {src.City}")))
                    .ForMember(c => c.OpeningHour,
                        opts => opts.MapFrom(
                            src => src.OpeningHour.ToString("HH:mm")));
                cfg.CreateMap<CinemaBl, Cinema>()
                    .ForMember(c => c.Auditoria, opt => opt.Ignore())
                    .ForMember(c => c.OpeningHour,
                    opts => opts.MapFrom(
                        src => src.OpeningHour.ToString("HH:mm")));
                cfg.CreateMap<Auditorium, AuditoriumBl>();
                cfg.CreateMap<AuditoriumBl, Auditorium>()
                    .ForMember(a => a.Cinema, opt => opt.Ignore())
                    .ForMember(a => a.Cinema_Id, opt => opt.Ignore());
                cfg.CreateMap<Screening, ScreeningBl>()
                    .ForMember(sbl => sbl.AuditoriumNumber, m => m.MapFrom(s => s.Auditorium.AuditoriumNumber))
                    .ForMember(sbl => sbl.AuditoriumId, m => m.MapFrom(s => s.Auditorium.Id));
                cfg.CreateMap<ScreeningBl, Screening>()
                    .ForMember(c => c.Auditorium, opt => opt.Ignore())
                    .ForMember(c => c.FilmScreening, opt => opt.Ignore())
                    .ForMember(c => c.FilmScreening_Id, opt => opt.Ignore());
                cfg.CreateMap<Programme, ProgrammeBl>()
                    .ForMember(pbl => pbl.StartDay,
                        opts => opts.MapFrom(
                            src => DateTime.ParseExact(src.Week.Substring(0, src.Week.IndexOf("-")), "dd/MM/yyyy",
                                CultureInfo.InvariantCulture)))
                    .ForMember(pbl => pbl.FilmScreeningsId, m => m.MapFrom(p => p.FilmScreenings.Select(f => f.Id)))
                    .ForMember(pbl => pbl.CinemaId, m => m.MapFrom(p => p.Cinema.Id));
                cfg.CreateMap<ProgrammeBl, Programme>()
                    .ForMember(p => p.Week,
                        opts => opts.MapFrom(
                            src => string.Format($"{src.StartDay:dd/MM/yyyy}-{src.StartDay.AddDays(6):dd/MM/yyyy}")))
                    .ForMember(p => p.FilmScreenings, opt => opt.Ignore());
                cfg.CreateMap<Ticket, TicketBl>()
                    .ForMember(tbl => tbl.FilmTitle, m => m.MapFrom(t => t.Film.Title));
                cfg.CreateMap<Ticket, TicketDetails>()
                    .ForMember(tbl => tbl.FilmTitle, m => m.MapFrom(t => t.Film.Title))
                    .ForMember(tbl => tbl.CinemaName, m => m.MapFrom(t => t.Cinema.Name))
                    .ForMember(tbl => tbl.CinemaAddress, m => m.MapFrom(t => t.Cinema.Address));
                cfg.CreateMap<TicketBl, Ticket>();
                cfg.CreateMap<FilmScreening, FilmScreeningBl>()
                    .ForMember(fbl => fbl.FilmId, m => m.MapFrom(s => s.Film.Id))
                    .ForMember(fbl => fbl.ScreeningsId, m => m.MapFrom(s => s.Screenings.Select(f => f.Id)));
                cfg.CreateMap<FilmScreeningBl, FilmScreening>()
                    .ForMember(f => f.Film, opt => opt.Ignore())
                    .ForMember(f => f.Screenings, opt => opt.Ignore())
                    .ForMember(f => f.Programme, opt => opt.Ignore())
                    .ForMember(f => f.Programme_Id, opt => opt.Ignore());
            });

            Mapper.AssertConfigurationIsValid();
            return Mapper.Instance;
        }
    }
}
