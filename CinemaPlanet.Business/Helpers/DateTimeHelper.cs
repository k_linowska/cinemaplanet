﻿using System;

namespace CinemaPlanet.Business.Helpers
{
    public interface IDateTimeHelper
    {
        string GetWeek(DateTime? screeningDay);
        bool IsFriday(DateTime screeningDay);
        string FindNextWeek(DateTime startDay);
        DateTime CombineDates(DateTime dayOfScreening, DateTime timeOfScreening);
    }

    public class DateTimeHelper : IDateTimeHelper
    {
        public string GetWeek(DateTime? screeningDay)
        {
            while (screeningDay?.DayOfWeek != DayOfWeek.Friday)
            {
                screeningDay = screeningDay?.AddDays(-1);
            }

            return $"{screeningDay:dd/MM/yyyy}-{screeningDay?.AddDays(6):dd/MM/yyyy}";
        }

        public bool IsFriday(DateTime screeningDay)
        {
            return screeningDay.DayOfWeek.Equals(DayOfWeek.Friday);
        }

        public string FindNextWeek(DateTime startDay)
        {
            if (startDay.DayOfWeek != DayOfWeek.Friday)
            {
                throw new Exception($"{startDay:dd/MM/yyyy} is not friday");
            }
            var daysUntilFriday = ((int)DayOfWeek.Friday - (int)startDay.AddDays(1).DayOfWeek + 7) % 7;
            return $"{startDay.AddDays(daysUntilFriday):dd/MM/yyyy}-{startDay.AddDays(daysUntilFriday + 6):dd/MM/yyyy}";
        }

        public DateTime CombineDates(DateTime dayOfScreening, DateTime timeOfScreening)
        {
            return new DateTime(dayOfScreening.Year, dayOfScreening.Month, dayOfScreening.Day,
                timeOfScreening.Hour, timeOfScreening.Minute, 0);
        }
    }
}
