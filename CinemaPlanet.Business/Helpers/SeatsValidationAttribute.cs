﻿using System;
using System.Collections;
using System.ComponentModel.DataAnnotations;

namespace CinemaPlanet.Business.Helpers
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = false)]
    public class SeatsValidationAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value is IEnumerable list)
            {
                RegularExpressionAttribute regex = new RegularExpressionAttribute("^(?<row>[A-Z])(?<seat>[1-9]{1,2})$");
                foreach (string str in list)
                {
                    if (!regex.IsValid(str))
                    {
                        return new ValidationResult("At least one element is not valid seat.");
                    }
                }

                return ValidationResult.Success;
            }

            return base.IsValid(value, validationContext);
        }
    }

}
