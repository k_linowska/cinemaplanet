﻿using CinemaPlanet.Business.Helpers;
using CinemaPlanet.Business.Services;
using CinemaPlanet.Data;
using Ninject;
using Ninject.Modules;

namespace CinemaPlanet.Business
{
    public class CinemaPlanetBusinessModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<ICinemasService>().To<CinemasService>();
            Kernel.Bind<IFilmsService>().To<FilmsService>();
            Kernel.Bind<IProgrammesService>().To<ProgrammesService>();
            Kernel.Bind<IProgrammeValidator>().To<ProgrammeValidator>();
            Kernel.Bind<IDateTimeHelper>().To<DateTimeHelper>();
            Kernel.Bind<IClientsService>().To<ClientsService>();
            Kernel.Bind<ITicketsService>().To<TicketsService>();
            Kernel.Bind<IPurchaseService>().To<PurchaseService>();
            Kernel.Load(new CinemaPlanetDataModule(), new AutoMapperModule());
        }
    }
}
