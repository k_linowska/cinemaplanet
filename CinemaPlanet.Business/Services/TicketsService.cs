﻿using AutoMapper;
using CinemaPlanet.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using CinemaPlanet.Business.Models;
using CinemaPlanet.Data.Models;
using CinemaPlanet.Business.Helpers;

namespace CinemaPlanet.Business.Services
{
    public interface ITicketsService
    {
        Task<PagedResults<TicketDetails>> GetTicketsHistoryPage(int pageSize, int page, int clientId);
        Task AddTicketAsync(PurchaseDetails purchase);
        Task<List<string>> GetUnavailableSeatsAsync(int screeningId, DateTime dateTimeOfScreening);
        int TicketsBoughtInSameMonth(List<TicketBl> userTickets);
        Task<List<TicketBl>> TicketsBoughtByUserAsync(int clientId);
    }

    public class TicketsService : ITicketsService
    {
        private readonly Func<ICinemaPlanetContext> _context;
        private readonly IMapper _mapper;
        private readonly IProgrammesService _programmesService;
        private readonly IFilmsService _filmsService;
        private readonly IDateTimeHelper _dateTimeHelper;

        public TicketsService(
            IMapper mapper,
            Func<ICinemaPlanetContext> context,
            IDateTimeHelper dateTimeHelper,
            IFilmsService filmsService,
            IProgrammesService programmesService)
        {
            _mapper = mapper;
            _context = context;
            _dateTimeHelper = dateTimeHelper;
            _filmsService = filmsService;
            _programmesService = programmesService;
        }

        public async Task<List<string>> GetUnavailableSeatsAsync(int screeningId, DateTime dateTimeOfScreening)
        {
            using (var ctx = _context())
            {
                var tickets = await ctx.Tickets
                    .Where(t => t.Screening_Id == screeningId && t.ScreeningDateTime == dateTimeOfScreening)
                    .ToListAsync();
                var listOfSeats = new List<string>();
                foreach (var ticket in tickets)
                {
                    listOfSeats.Add(ticket.ClientSeat);
                }

                return listOfSeats;
            }
        }

        public int TicketsBoughtInSameMonth(List<TicketBl> userTickets)
        {
            int ticketsBoughtInSameMonth = 0;
            foreach (var ticket in userTickets)
            {
                if (ticket.TimeOfPurchase.Month == DateTime.Now.Month &&
                    ticket.TimeOfPurchase.Year == DateTime.Now.Year)
                {
                    ticketsBoughtInSameMonth++;
                }
            }

            return ticketsBoughtInSameMonth;
        }

        public async Task<List<TicketBl>> TicketsBoughtByUserAsync(int clientId)
        {
            using (var ctx = _context())
            {
                return _mapper.Map<List<TicketBl>>(await ctx.Tickets
                    .Where(t => t.Client_Id == clientId).ToListAsync());
            }
        }

        public async Task<PagedResults<TicketDetails>> GetTicketsHistoryPage(int pageSize, int page, int clientId)
        {
            using (var ctx = _context())
            {
                var tickets = await ctx.Tickets.Include(t => t.Cinema).Include(t => t.Film).Where(t => t.Client.Id == clientId).ToListAsync();
                var resultCount = tickets.Count();
                var pagesCount = (int)Math.Ceiling((double)resultCount / (double)pageSize);
                var resultItems = tickets
                .Skip(pageSize * (page - 1))
                .Take(pageSize)
                .Select(
                x => new TicketDetails
                {
                    CinemaAddress = x.Cinema.Address,
                    CinemaName = x.Cinema.Name,
                    ClientSeat = x.ClientSeat,
                    FilmTitle = x.Film.Title,
                    ScreeningDateTime = x.ScreeningDateTime,
                    TimeOfPurchase = x.TimeOfPurchase
                })
                .ToList();

                var results = new PagedResults<TicketDetails>
                {
                    Page = page,
                    PageSize = pageSize,
                    ResultsCount = resultCount,
                    PagesCount = pagesCount,
                    NextPageAttributes = (pagesCount <= page) ? null : $"?pageSize={pageSize}&page={page + 1}",
                    PrevPageAttributes = (page <= 1) ? null : $"?pageSize={pageSize}&page={page - 1}",
                    Results = resultItems
                };

                return results;
            }
        }

        public async Task AddTicketAsync(PurchaseDetails purchase)
        {
            using (var ctx = _context())
            {
                var cinema = await ctx.Cinemas.FindAsync(purchase.Cinema.Id);
                var client = await ctx.Clients.FindAsync(purchase.ClientId);
                var film = await ctx.Films.FindAsync(purchase.Film.Id);
                var screening = await ctx.Screenings.FindAsync(purchase.ScreeningId);
                var timeOfPurchase = DateTime.Now;

                foreach (var seat in purchase.Seats)
                {
                    var ticket = new Ticket
                    {
                        Cinema = cinema,
                        Client_Id = purchase.ClientId,
                        Client = client,
                        ClientSeat = seat,
                        Film = film,
                        Screening = screening,
                        Screening_Id = purchase.ScreeningId,
                        ScreeningDateTime = purchase.DateTimeOfScreening,
                        TimeOfPurchase = timeOfPurchase
                    };
                    ctx.Tickets.Add(ticket);
                }

                await ctx.SaveChangesAsync();
            }
        }
    }
}
