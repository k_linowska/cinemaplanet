﻿using AutoMapper;
using CinemaPlanet.Data;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using CinemaPlanet.Business.Models;
using CinemaPlanet.Data.Models;

namespace CinemaPlanet.Business.Services
{
    public interface IClientsService
    {
        Task<bool> IsEmailTaken(string email);
        Task<ClientDetails> AuthorizeUser(ClientAuthData user);
        Task AddCLient(ClientBl client);
        Task AddCLient(ClientRegistration client);
        Task UpdateCLient(ClientBl client);
        Task<ClientBl> GetCLient(string email);
        Task<ClientBl> GetCLient(int clientId);
    }

    public class ClientsService : IClientsService
    {
        private readonly Func<ICinemaPlanetContext> _context;
        private readonly IMapper _mapper;

        public ClientsService(IMapper mapper, Func<ICinemaPlanetContext> context)
        {
            _mapper = mapper;
            _context = context;
        }

        public async Task<bool> IsEmailTaken(string email)
        {
            using (var ctx = _context())
            {
                return await ctx.Clients.AnyAsync(c => c.Email == email);
            }
        }

        public async Task AddCLient(ClientBl client)
        {
            using (var ctx = _context())
            {
                ctx.Clients.Add(_mapper.Map<Client>(client));
                await ctx.SaveChangesAsync();
            }
        }

        public async Task AddCLient(ClientRegistration client)
        {
            using (var ctx = _context())
            {
                ctx.Clients.Add(_mapper.Map<Client>(client));
                await ctx.SaveChangesAsync();
            }
        }

        public async Task UpdateCLient(ClientBl client)
        {
            using (var ctx = _context())
            {
                var clientData = await ctx.Clients.Include(c => c.Tickets).FirstOrDefaultAsync(c => c.Id == client.Id);

                foreach (var ticket in client.Tickets)
                {
                    clientData.Tickets.Add(ctx.Tickets.Add(_mapper.Map<Ticket>(ticket)));
                }

                await ctx.SaveChangesAsync();
            }
        }

        public async Task<ClientBl> GetCLient(string email)
        {
            using (var ctx = _context())
            {
                var x = _mapper.Map<ClientBl>(await ctx.Clients.Include(c => c.Tickets).FirstOrDefaultAsync(c => c.Email == email));
                return _mapper.Map<ClientBl>(await ctx.Clients.Include(c => c.Tickets).FirstOrDefaultAsync(c => c.Email == email));
            }
        }

        public async Task<ClientDetails> AuthorizeUser(ClientAuthData user)
        {
            using (var ctx = _context())
            {
                var userData = await ctx.Clients.SingleOrDefaultAsync(x => x.Email == user.Email && x.PasswordHash.ToUpper() == user.PasswordHash.ToUpper());
                if (userData == null)
                {
                    return null;
                }
                var userToReturn = new ClientDetails
                {
                    Email = userData.Email,
                    Id = userData.Id,
                    PasswordHash = userData.PasswordHash
                };
                return userToReturn;
            }
        }

        public async Task<ClientBl> GetCLient(int clientId)
        {
            using (var ctx = _context())
            {
                return _mapper.Map<ClientBl>(await ctx.Clients.Include(c => c.Tickets.Select(t => t.Film))
                    .Include(c => c.Tickets.Select(t => t.Cinema))
                    .FirstOrDefaultAsync(c => c.Id == clientId));
            }
        }
    }
}
