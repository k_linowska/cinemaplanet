﻿using AutoMapper;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.IO;
using System.Xml.Serialization;
using CinemaPlanet.Data;
using CinemaPlanet.Business.Models;
using CinemaPlanet.Data.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CinemaPlanet.Business.Services
{
    public interface IProgrammesService
    {
        Task AddFilmScreeningAsync(FilmScreeningBl filmScreeningsBl);
        Task AddProgrammeAsync(ProgrammeBl programmeBl);
        Task AddScreeningAsync(ScreeningBl screeningBl);
        Task<bool> DoesProgrammeExistAsync(string week);
        Task<FilmScreeningBl> FindFilmScreeningAsync(int programmeId, int filmId);
        Task<List<ProgrammeBl>> FindListOfProgrammesAsync(string week);
        Task<ProgrammeBl> FindProgrammeAsync(int cinemaId, int filmId);
        Task<ProgrammeBl> FindProgrammeAsync(int screeningId);
        Task<ProgrammeBl> FindProgrammeAsync(string week, int cinemaId);
        Task<FilmScreeningBl> GetFilmScreeningsBlAsync(int filmScreeningsId);
        Task<ScreeningBl> GetScreeningBlAsync(int screeningId);
        Task SerializeProgrammeAsync(string format, string path, int programmeId);
        Task UpdateProgrammeAsync(ProgrammeBl programme);
    }

    public class ProgrammesService : IProgrammesService
    {
        private readonly Func<ICinemaPlanetContext> _context;
        private readonly IMapper _mapper;

        public ProgrammesService(Func<ICinemaPlanetContext> context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task AddFilmScreeningAsync(FilmScreeningBl filmScreeningBl)
        {
            if (filmScreeningBl.Id != 0)
            {
                await UpdateFilmScreeningAsync(filmScreeningBl);
                return;
            }

            var filmScreeningsData = _mapper.Map<FilmScreening>(filmScreeningBl);

            using (var ctx = _context())
            {
                filmScreeningsData.Film = await ctx.Films.FindAsync(filmScreeningBl.FilmId);

                foreach (var screeningId in filmScreeningBl.ScreeningsId)
                {
                    filmScreeningsData.Screenings.Add(await ctx.Screenings.FindAsync(screeningId));
                }

                ctx.FilmScreenings.Add(filmScreeningsData);
                await ctx.SaveChangesAsync();
            }

            filmScreeningBl.Id = filmScreeningsData.Id;
        }

        public async Task AddProgrammeAsync(ProgrammeBl programmeBl)
        {
            var programmeData = _mapper.Map<Programme>(programmeBl);

            using (var ctx = _context())
            {
                programmeData.Cinema = await ctx.Cinemas.FindAsync(programmeBl.CinemaId);

                foreach (var filmScreening in programmeBl.FilmScreenings)
                {
                    var filmScreeningData = ctx.FilmScreenings.Add(_mapper.Map<FilmScreening>(filmScreening));
                    filmScreeningData.Film = await ctx.Films.FindAsync(filmScreening.FilmId);

                    foreach (var screening in filmScreening.Screenings)
                    {
                        var screeningData = _mapper.Map<Screening>(screening);
                        var auditoriumTest = await ctx.Auditoria.FirstOrDefaultAsync(a =>
                            a.Cinema_Id == programmeBl.CinemaId && a.AuditoriumNumber == screening.AuditoriumNumber);

                        screeningData.Auditorium = await ctx.Auditoria.FindAsync(auditoriumTest.Id);
                        ctx.Screenings.Add(screeningData);
                        filmScreeningData.Screenings.Add(screeningData);
                    }

                    programmeData.FilmScreenings.Add(filmScreeningData);
                }

                ctx.Programmes.Add(programmeData);
                await ctx.SaveChangesAsync();
            }

            programmeBl.Id = programmeData.Id;
        }

        public async Task AddScreeningAsync(ScreeningBl screeningBl)
        {
            var screeningData = _mapper.Map<Screening>(screeningBl);

            using (var ctx = _context())
            {
                screeningData.Auditorium = await ctx.Auditoria.FindAsync(screeningBl.AuditoriumId);
                ctx.Screenings.Add(screeningData);
                await ctx.SaveChangesAsync();
            }

            screeningBl.Id = screeningData.Id;
        }

        public async Task<bool> DoesProgrammeExistAsync(string week)
        {
            using (var ctx = _context())
            {
                return await ctx.Programmes.AnyAsync(p => p.Week == week);
            }
        }

        public async Task<FilmScreeningBl> FindFilmScreeningAsync(int programmeId, int filmId)
        {
            using (var ctx = _context())
            {
                var programme = await ctx.Programmes
                    .Include(x => x.FilmScreenings.Select(y => y.Film))
                    .Include(x => x.FilmScreenings.Select(w => w.Screenings))
                    .FirstOrDefaultAsync(p => p.Id == programmeId);
                var filmScreening = programme?.FilmScreenings.FirstOrDefault(f => f.Film.Id == filmId);

                return _mapper.Map<FilmScreeningBl>(filmScreening);
            }
        }

        public async Task<List<ProgrammeBl>> FindListOfProgrammesAsync(string week)
        {
            using (var ctx = _context())
            {
                return _mapper.Map<List<ProgrammeBl>>(await ctx.Programmes
                    .Include(p => p.Cinema)
                    .Include(p => p.FilmScreenings.Select(f => f.Film))
                    .Include(p => p.FilmScreenings.Select(f => f.Screenings))
                    .Where(p => p.Week == week).ToListAsync());
            }
        }

        public async Task<ProgrammeBl> FindProgrammeAsync(int cinemaId, int filmId)
        {
            using (var ctx = _context())
            {
                var programmes =
                    from program in await ctx.Programmes.Include(p => p.Cinema).Where(p => p.Cinema.Id == cinemaId)
                        .ToListAsync()
                    join filmScreening in ctx.FilmScreenings on program.Id equals filmScreening.Programme_Id
                    where filmScreening.Film.Id == filmId
                    orderby program.Week descending
                    select program;

                if (programmes.ToList().Count == 0)
                {
                    return null;
                }

                return _mapper.Map<ProgrammeBl>(programmes.ToList().ElementAt(0));
            }
        }

        public async Task<ProgrammeBl> FindProgrammeAsync(int screeningId)
        {
            using (var ctx = _context())
            {
                var programmes =
                    from program in await ctx.Programmes.Include(p => p.Cinema.Auditoria).Include(p => p.FilmScreenings).ToListAsync()
                    join filmScreening in ctx.FilmScreenings.Include(f => f.Film) on program.Id equals filmScreening.Programme_Id
                    join screening in ctx.Screenings on filmScreening.Id equals screening.FilmScreening_Id
                    where screening.Id == screeningId
                    select program;

                return _mapper.Map<ProgrammeBl>(programmes.ToList()[0]);
            }
        }

        public async Task<ProgrammeBl> FindProgrammeAsync(string week, int cinemaId)
        {
            using (var ctx = _context())
            {
                var programme = await ctx.Programmes
                    .Include(p => p.Cinema)
                    .Include(p => p.FilmScreenings.Select(f => f.Film))
                    .Include(p => p.FilmScreenings.Select(f => f.Screenings.Select(s => s.Auditorium)))
                    .FirstOrDefaultAsync(p => p.Cinema.Id == cinemaId && p.Week == week);

                return _mapper.Map<ProgrammeBl>(programme);
            }
        }

        public async Task<FilmScreeningBl> GetFilmScreeningsBlAsync(int filmScreeningsId)
        {
            using (var ctx = _context())
            {
                return _mapper.Map<FilmScreeningBl>(await ctx.FilmScreenings
                    .Include(f => f.Film)
                    .Include(x => x.Screenings)
                    .FirstOrDefaultAsync(f => f.Id == filmScreeningsId));
            }
        }

        public async Task<ScreeningBl> GetScreeningBlAsync(int screeningId)
        {
            using (var ctx = _context())
            {
                return _mapper.Map<ScreeningBl>(await ctx.Screenings.Include(s => s.Auditorium).FirstOrDefaultAsync(s => s.Id == screeningId));
            }
        }

        private async Task UpdateFilmScreeningAsync(FilmScreeningBl filmScreeningsBl)
        {
            using (var ctx = _context())
            {
                var filmScreeningsData = await ctx.FilmScreenings.FindAsync(filmScreeningsBl.Id);

                foreach (var screeningId in filmScreeningsBl.ScreeningsId)
                {
                    if (filmScreeningsData != null && !filmScreeningsData.Screenings.Contains(await ctx.Screenings.FindAsync(screeningId)))
                    {
                        filmScreeningsData.Screenings.Add(await ctx.Screenings.FindAsync(screeningId));
                    }
                }

                await ctx.SaveChangesAsync();
            }
        }

        public async Task UpdateProgrammeAsync(ProgrammeBl programme)
        {
            using (var ctx = _context())
            {
                var programmeData = await ctx.Programmes.FindAsync(programme.Id);

                foreach (var filmScreeningId in programme.FilmScreeningsId)
                {
                    if (programmeData != null && !programmeData.FilmScreenings.Contains(await ctx.FilmScreenings.FindAsync(filmScreeningId)))
                    {
                        programmeData.FilmScreenings.Add(await ctx.FilmScreenings.FindAsync(filmScreeningId));
                    }
                }

                await ctx.SaveChangesAsync();
            }
        }

        public async Task SerializeProgrammeAsync(string format, string path, int programmeId)
        {
            Programme programme;
            using (var ctx = _context())
            {
                programme = await ctx.Programmes.Include(p => p.Cinema)
                                   .Include(p => p.FilmScreenings.Select(f => f.Film))
                                   .Include(p => p.FilmScreenings.Select(f => f.Screenings.Select(s => s.Auditorium)))
                                   .FirstOrDefaultAsync(p => p.Id == programmeId);
            }

            if (format == "json")
            {
                SerializeJson(path, _mapper.Map<ProgrammeBl>(programme));
            }
            if (format == "xml")
            {
                SerializeXml(path, _mapper.Map<ProgrammeBl>(programme));
            }
        }

        private void SerializeXml(string path, ProgrammeBl programme)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(ProgrammeBl));
            StringBuilder stringBuilder = new StringBuilder();
            serializer.Serialize(new StringWriter(stringBuilder), programme);
            File.WriteAllText(path + ".xml", stringBuilder.ToString());
        }

        private void SerializeJson(string path, ProgrammeBl programme)
        {
            var json = JsonConvert.SerializeObject(programme);
            File.WriteAllText(path + ".json", json);
        }
    }
}
