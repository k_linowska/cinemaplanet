﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CinemaPlanet.Business.Models;
using CinemaPlanet.Data;
using CinemaPlanet.Data.Models;

namespace CinemaPlanet.Business.Services
{
    public interface IFilmsService
    {
        Task AddFilmAsync(FilmBl film);
        Task<List<FilmBl>> GetAllFilmsAsync();
        Task<FilmBl> GetFilmBlAsync(int filmId);
        Task<bool> DoesFilmExistAsync(int filmId);
        Task<FilmBl> FindFilmAsync(int screeningId);
    }

    public class FilmsService : IFilmsService
    {
        private readonly Func<ICinemaPlanetContext> _context;
        private readonly IMapper _mapper;

        public FilmsService(Func<ICinemaPlanetContext> context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<FilmBl> GetFilmBlAsync(int filmId)
        {
            using (var ctx = _context())
            {
                return _mapper.Map<FilmBl>(await ctx.Films.FindAsync(filmId));
            }
        }

        public async Task<bool> DoesFilmExistAsync(int filmId)
        {
            using (var ctx = _context())
            {
                if (await ctx.Films.FindAsync(filmId) == null)
                {
                    return false;
                }

                return true;
            }
        }

        public async Task AddFilmAsync(FilmBl film)
        {
            using (var ctx = _context())
            {
                if (await ctx.Films.AnyAsync(f => f.Website == film.Website))
                {
                    return;
                }

                ctx.Films.Add(_mapper.Map<Film>(film));
                await ctx.SaveChangesAsync();
            }
        }

        public async Task<List<FilmBl>> GetAllFilmsAsync()
        {
            using (var ctx = _context())
            {
                return (await ctx.Films.ToListAsync()).Select(f => _mapper.Map<FilmBl>(f)).ToList();
            }
        }

        public async Task<FilmBl> FindFilmAsync(int screeningId)
        {
            using (var ctx = _context())
            {
                var filmScreeningToFind =
                    from filmScreening in await ctx.FilmScreenings.Include(f => f.Film).ToListAsync()
                    join screening in ctx.Screenings on filmScreening.Id equals screening.FilmScreening_Id
                    where screening.Id == screeningId
                    select filmScreening;

                return _mapper.Map<FilmBl>(filmScreeningToFind.ToList()[0].Film);
            }
        }
    }
}
