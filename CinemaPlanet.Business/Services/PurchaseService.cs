﻿using AutoMapper;
using CinemaPlanet.Business.Helpers;
using CinemaPlanet.Business.Models;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace CinemaPlanet.Business.Services
{
    public interface IPurchaseService
    {
        Task<bool> IsPurchaseValidAsync(PurchaseDetails purchase, DateTime screeningDate);
        Task<bool> IsSeatAvailableAsync(string chosenSeat, List<string> chosenSeats, int screeningId, DateTime dateTimeOfScreening);
        void SendReceipt(PurchaseDetails purchase);
        Task<decimal> SetPriceAsync(int clientId, int count, DateTime dateTimeOfScreening);
    }

    public class PurchaseService : IPurchaseService
    {
        private readonly IClientsService _clientsService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IMapper _mapper;
        private readonly ITicketsService _ticketsService;
        private readonly IFilmsService _filmsService;
        private readonly IProgrammesService _programmesService;
        private readonly ICinemasService _cinemasService;

        public PurchaseService(
            IMapper mapper,
            IClientsService clientsService,
            IFilmsService filmsService,
            ITicketsService ticketsService,
            IDateTimeHelper dateTimeHelper,
            IProgrammesService programmesService,
            ICinemasService cinemasService)
        {
            _mapper = mapper;
            _clientsService = clientsService;
            _filmsService = filmsService;
            _ticketsService = ticketsService;
            _dateTimeHelper = dateTimeHelper;
            _programmesService = programmesService;
            _cinemasService = cinemasService;
        }

        public async Task<bool> IsPurchaseValidAsync(PurchaseDetails purchase, DateTime screeningDate)
        {
            var client = await _clientsService.GetCLient(purchase.ClientId);
            if (client == null)
            {
                return false;
            }

            if (screeningDate < DateTime.Now)
            {
                return false;
            }

            purchase.Client = client;
            var screening = await _programmesService.GetScreeningBlAsync(purchase.ScreeningId);
            if (screening == null)
            {
                return false;
            }

            purchase.DateTimeOfScreening = _dateTimeHelper.CombineDates(screeningDate, screening.TimeOfScreening);
            var programme = await _programmesService.FindProgrammeAsync(screening.Id);

            if (screeningDate < programme.StartDay || screeningDate > programme.StartDay.AddDays(6))
            {
                return false;
            }

            if (!await AreSeatsValidAsync(purchase, await _cinemasService.GetAuditoriumBlAsync(screening.AuditoriumId)))
            {
                return false;
            }

            purchase.Price = await SetPriceAsync(purchase.ClientId, purchase.Seats.Count, purchase.DateTimeOfScreening);
            purchase.Cinema = programme.Cinema;
            purchase.Film = await _filmsService.FindFilmAsync(screening.Id);
            return true;
        }


        private async Task<bool> AreSeatsValidAsync(PurchaseDetails purchase, AuditoriumBl auditorium)
        {
            var chosenSeats = new List<string>();
            foreach (var seat in purchase.Seats)
            {
                if (!IsSeatValidForAuditorium(seat, auditorium))
                {
                    return false;
                }

                if (!await IsSeatAvailableAsync(seat, chosenSeats, purchase.ScreeningId, purchase.DateTimeOfScreening))
                {
                    return false;
                }

                chosenSeats.Add(seat);
            }

            return true;
        }

        public bool IsSeatValidForAuditorium(string chosenSeat, AuditoriumBl auditorium)
        {
            const string letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var row = chosenSeat[0];
            var seat = Convert.ToInt32(chosenSeat.Substring(1));

            if (letters.IndexOf(row) + 1 > auditorium.Rows)
            {
                return false;
            }

            if (seat > auditorium.SeatsInRow)
            {
                return false;
            }

            return true;
        }

        public async Task<bool> IsSeatAvailableAsync(string chosenSeat, List<string> chosenSeats, int screeningId, DateTime dateTimeOfScreening)
        {
            var x = await _ticketsService.GetUnavailableSeatsAsync(screeningId, dateTimeOfScreening);
            chosenSeats.AddRange(await _ticketsService.GetUnavailableSeatsAsync(screeningId, dateTimeOfScreening));
            if (IsSeatTaken(chosenSeat, chosenSeats))
            {
                return false;
            }

            return true;
        }

        public bool IsSeatTaken(string chosenSeat, List<string> chosenSeats)
        {
            foreach (var seat in chosenSeats)
            {
                if (seat == chosenSeat)
                {
                    return true;
                }
            }

            return false;
        }

        public async Task<decimal> SetPriceAsync(int clientId, int seatsReserved, DateTime dateTimeOfScreening)
        {
            var userTickets = await _ticketsService.TicketsBoughtByUserAsync(clientId);
            var ticketsBoughtInSameMonthCount = _ticketsService.TicketsBoughtInSameMonth(userTickets);
            var price = SetPriceWithDiscounts(seatsReserved, dateTimeOfScreening, userTickets.Count, ticketsBoughtInSameMonthCount);

            return price;
        }

        public decimal SetPriceWithDiscounts(int seatsReserved, DateTime dateTimeOfScreening, int userTicketsCount, int ticketsBoughtInSameMonthCount)
        {
            decimal price = 0;
            for (int i = 1; i <= seatsReserved; i++)
            {
                if (dateTimeOfScreening.DayOfWeek == DayOfWeek.Friday
                    || (dateTimeOfScreening.DayOfWeek == DayOfWeek.Saturday
                        || (dateTimeOfScreening.DayOfWeek == DayOfWeek.Sunday)))
                {
                    if (CheckFreeTicketDiscount(userTicketsCount, i))
                    {
                        continue;
                    }

                    if (Check20PercentOffDiscount(ticketsBoughtInSameMonthCount, i))
                    {
                        price += 0.8M * 25;
                        continue;
                    }

                    price += 25;
                }
                else
                {
                    if (CheckFreeTicketDiscount(userTicketsCount, i))
                    {
                        continue;
                    }

                    if (Check20PercentOffDiscount(ticketsBoughtInSameMonthCount, i))
                    {
                        price += 0.8M * 20;
                        continue;
                    }

                    price += 20;
                }
            }

            return price;
        }

        private bool Check20PercentOffDiscount(int ticketsBoughtInSameMonth, int i)
        {
            if (ticketsBoughtInSameMonth + i > 5)
            {
                return true;
            }

            return false;
        }

        private bool CheckFreeTicketDiscount(int count, int i)
        {
            if ((count + i) % 10 == 0)
            {
                return true;
            }

            return false;
        }

        public void SendReceipt(PurchaseDetails purchase)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("localhost");

                mail.From = new MailAddress("test@gmail.com");
                mail.To.Add(purchase.Client.Email);
                mail.Subject = $"Reservation for screening on {purchase.DateTimeOfScreening}";
                mail.Body = SetEmailBody(purchase);
                mail.BodyEncoding = Encoding.UTF8;
                SmtpServer.Port = 2500;
                SmtpServer.Send(mail);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private string SetEmailBody(PurchaseDetails purchase)
        {
            return $"Film title: {purchase.Film.Title}\n" +
                $"Screening date: {purchase.DateTimeOfScreening}\n" +
                $"Cinema: {purchase.Cinema.Name}, address: {purchase.Cinema.Address}\n" +
                $"Number of tickets: {purchase.Seats.Count}\n" +
                $"Reserved seats: {String.Join(", ", purchase.Seats)}\n" +
                $"Price: {purchase.Price}";
        }
    }
}
