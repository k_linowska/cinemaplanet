﻿using System;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using CinemaPlanet.Business.Helpers;
using CinemaPlanet.Business.Models;
using CinemaPlanet.Data;
using CinemaPlanet.Data.Models;

namespace CinemaPlanet.Business.Services
{
    public interface IProgrammeValidator
    {
        Task<bool> DoesProgrammeExistAsync(int programmeId);
        bool DoesScreeningStartsAfterOpeningHour(DateTime timeToValidate, string openingHour);
        bool IsScreeningTimeValid(DateTime timeToValidate, int filmsRuntimeToValidate,
            DateTime screeningTimeToCompare, int filmsRuntimeToCompare);
        Task<bool> IsTimeAndAuditoriumAvailableAsync(DateTime timeOfScreening, int auditoriumNumber,
            int programmeId, int filmId);
        Task<bool> IsTimeAndAuditoriumAvailableAsync(DateTime timeOfScreening, int auditoriumNumber,
            ProgrammeBl programme, int filmId);
        Task<bool> IsProgrammeValid(ProgrammeBl programmeBl);
    }

    public class ProgrammeValidator : IProgrammeValidator
    {
        private readonly Func<ICinemaPlanetContext> _context;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly ICinemasService _cinemasService;
        private readonly IFilmsService _filmsService;
        private readonly IProgrammesService _programmesService;

        public ProgrammeValidator(
            Func<ICinemaPlanetContext> context,
            IDateTimeHelper dateTimeHelper,
            ICinemasService cinemasService,
            IFilmsService filmsService,
            IProgrammesService programmesService)
        {
            _context = context;
            _dateTimeHelper = dateTimeHelper;
            _cinemasService = cinemasService;
            _filmsService = filmsService;
            _programmesService = programmesService;
        }

        public async Task<bool> DoesProgrammeExistAsync(int programmeId)
        {
            using (var ctx = _context())
            {
                return await ctx.Programmes.AnyAsync(p => p.Id == programmeId);
            }
        }

        public bool DoesScreeningStartsAfterOpeningHour(DateTime timeToValidate, string openingHour)
        {
            var x = DateTime.ParseExact(openingHour, "HH:mm", CultureInfo.InvariantCulture);
            var z = x.Ticks;
            if (timeToValidate.TimeOfDay < DateTime.ParseExact(openingHour, "HH:mm", CultureInfo.InvariantCulture)
                    .TimeOfDay)
            {
                return false;
            }

            return true;
        }

        public async Task<bool> IsTimeAndAuditoriumAvailableAsync(DateTime timeOfScreening, int auditoriumNumber,
           int programmeId, int filmId)
        {
            using (var ctx = _context())
            {
                var programme = await ctx.Programmes
                                   .Include(p => p.Cinema.Auditoria)
                                   .Include(p => p.FilmScreenings
                                        .Select(f => f.Film))
                                   .Include(p => p.FilmScreenings
                                        .Select(f => f.Screenings
                                        .Select(s => s.Auditorium)))
                                   .FirstOrDefaultAsync(p => p.Id == programmeId);

                if (!await IsAuditoriumCorrectForCinemaAsync(auditoriumNumber, programme.Cinema.Id))
                {
                    return false;
                }

                var filmsRuntimeToValidate = (await ctx.Films.FindAsync(filmId)).RunningTime;

                if (!IsScreeningTimeAvailable(auditoriumNumber, programme, timeOfScreening, filmsRuntimeToValidate))
                {
                    return false;
                }

                return true;
            }
        }

        public async Task<bool> IsTimeAndAuditoriumAvailableAsync(DateTime timeOfScreening, int auditoriumNumber,
            ProgrammeBl programme, int filmId)
        {
            using (var ctx = _context())
            {
                if (!await IsAuditoriumCorrectForCinemaAsync(auditoriumNumber, programme.CinemaId))
                {
                    return false;
                }

                var filmsRuntimeToValidate = (await ctx.Films.FindAsync(filmId)).RunningTime;

                if (!await IsScreeningTimeAvailable(auditoriumNumber, programme, timeOfScreening, filmsRuntimeToValidate))
                {
                    return false;
                }

                return true;
            }
        }

        private async Task<bool> IsAuditoriumCorrectForCinemaAsync(int auditoriumNumber, int cinemaId)
        {
            using (var ctx = _context())
            {
                return await ctx.Cinemas
                    .Include(c => c.Auditoria)
                    .AnyAsync(c => c.Id == cinemaId && c.Auditoria.Any(a => a.AuditoriumNumber == auditoriumNumber));
            }
        }
        public async Task<bool> IsProgrammeValid(ProgrammeBl programmeBl)
        {
            if (!_dateTimeHelper.IsFriday(programmeBl.StartDay))
            {
                return false;
            }

            if (await _cinemasService.GetCinemaBlAsync(programmeBl.CinemaId) == null)
            {
                return false;
            }

            if (await _programmesService.FindProgrammeAsync(_dateTimeHelper.GetWeek(programmeBl.StartDay),
                    programmeBl.CinemaId) != null)
            {
                return false;
            }

            foreach (var filmScreening in programmeBl.FilmScreenings)
            {
                if (!await _filmsService.DoesFilmExistAsync(filmScreening.FilmId))
                {
                    return false;
                }
            }

            return await ValidateScreeningHours(programmeBl);
        }

        private async Task<bool> ValidateScreeningHours(ProgrammeBl programmeBl)
        {
            var programmeToValidate = new ProgrammeBl { CinemaId = programmeBl.CinemaId };

            foreach (var filmScreening in programmeBl.FilmScreenings)
            {
                var filmScreeningToValidate = new FilmScreeningBl { FilmId = filmScreening.FilmId };
                programmeToValidate.FilmScreenings.Add(filmScreeningToValidate);

                foreach (var screening in filmScreening.Screenings)
                {
                    if (!await IsTimeAndAuditoriumAvailableAsync(screening.TimeOfScreening,
                        screening.AuditoriumNumber, programmeToValidate, filmScreening.FilmId))
                    {
                        return false;
                    }

                    var screeningToValidate = new ScreeningBl
                    { AuditoriumNumber = screening.AuditoriumNumber, TimeOfScreening = screening.TimeOfScreening };
                    filmScreeningToValidate.Screenings.Add(screeningToValidate);
                }
            }

            return true;
        }

        private bool IsScreeningTimeAvailable(int auditoriumNumber, Programme programme, DateTime timeToValidate, int filmsRuntimeToValidate)
        {
            foreach (var filmScreening in programme.FilmScreenings)
            {
                if (!DoesScreeningStartsAfterOpeningHour(timeToValidate, programme.Cinema.OpeningHour))
                {
                    return false;
                }

                foreach (var screening in filmScreening.Screenings)
                {
                    var screeningTimeToCompare = screening.TimeOfScreening;
                    var filmsRuntimeToCompare = filmScreening.Film.RunningTime;

                    if (screening.Auditorium.AuditoriumNumber != auditoriumNumber)
                    {
                        continue;
                    }

                    if (!IsScreeningTimeValid(timeToValidate, filmsRuntimeToValidate, screeningTimeToCompare,
                        filmsRuntimeToCompare))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private async Task<bool> IsScreeningTimeAvailable(int auditoriumNumber, ProgrammeBl programme, DateTime timeToValidate, int filmsRuntimeToValidate)
        {
            string openingHour;
            using (var ctx = _context())
            {
                var cinema = await ctx.Cinemas.FindAsync(programme.CinemaId);
                openingHour = cinema.OpeningHour;
            }

            foreach (var filmScreening in programme.FilmScreenings)
            {
                if (!DoesScreeningStartsAfterOpeningHour(timeToValidate, openingHour))
                {
                    return false;
                }

                foreach (var screening in filmScreening.Screenings)
                {
                    var screeningTimeToCompare = screening.TimeOfScreening;
                    int filmsRuntimeToCompare;

                    using (var ctx = _context())
                    {
                        var film = await ctx.Films.FindAsync(filmScreening.FilmId);
                        filmsRuntimeToCompare = film.RunningTime;
                    }

                    if (screening.AuditoriumNumber != auditoriumNumber)
                    {
                        continue;
                    }

                    if (!IsScreeningTimeValid(timeToValidate, filmsRuntimeToValidate, screeningTimeToCompare,
                        filmsRuntimeToCompare))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        public bool IsScreeningTimeValid(DateTime timeToValidate, int filmsRuntimeToValidate,
            DateTime screeningTimeToCompare, int filmsRuntimeToCompare)
        {
            var breakBetweenScreenings = 10;

            if (timeToValidate.TimeOfDay >= screeningTimeToCompare.TimeOfDay &&
                timeToValidate.TimeOfDay < screeningTimeToCompare.AddMinutes(filmsRuntimeToCompare + breakBetweenScreenings).TimeOfDay)
            {
                return false;
            }

            if (timeToValidate.AddMinutes(filmsRuntimeToValidate + breakBetweenScreenings).TimeOfDay >= screeningTimeToCompare.TimeOfDay &&
                timeToValidate.AddMinutes(filmsRuntimeToValidate + breakBetweenScreenings).TimeOfDay <
                screeningTimeToCompare.AddMinutes(filmsRuntimeToCompare + breakBetweenScreenings).TimeOfDay)
            {
                return false;
            }

            if (screeningTimeToCompare.AddMinutes(filmsRuntimeToCompare + breakBetweenScreenings).TimeOfDay >= timeToValidate.TimeOfDay &&
                screeningTimeToCompare.AddMinutes(filmsRuntimeToCompare + breakBetweenScreenings).TimeOfDay <
                timeToValidate.AddMinutes(filmsRuntimeToValidate + breakBetweenScreenings).TimeOfDay)
            {
                return false;
            }

            return true;
        }
    }
}
