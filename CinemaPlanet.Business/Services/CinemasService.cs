﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CinemaPlanet.Business.Models;
using CinemaPlanet.Data;
using CinemaPlanet.Data.Models;
using System.Data.Entity;
using System.Threading.Tasks;

namespace CinemaPlanet.Business.Services
{
    public interface ICinemasService
    {
        Task AddAuditoriumAsync(AuditoriumBl auditoriumBl);
        Task AddCinemaAsync(CinemaBlToAdd cinema);
        Task<List<CinemaBl>> GetAllCinemasAsync();
        Task<List<CinemaGeneralInfo>> GetAllCinemasWithGeneralInfoAsync();
        Task<AuditoriumBl> GetAuditoriumBlAsync(int auditoriumId);
        Task<AuditoriumView> GetAuditoriumViewAsync(int screeningId);
        Task<CinemaBl> GetCinemaBlAsync(int cinemaId);
        Task<List<AuditoriumBl>> GetCinemasAuditoriaAsync(int cinemaId);
        Task UpdateCinemaAsync(int cinemaId, AuditoriumBl auditoriumBl);
        Task<bool> IsAuditoriumNumberAvailableAsync(int cinemaId, AuditoriumBl auditoriumBl);
    }

    public class CinemasService : ICinemasService
    {
        private readonly Func<ICinemaPlanetContext> _context;
        private readonly IMapper _mapper;

        public CinemasService(Func<ICinemaPlanetContext> context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task AddAuditoriumAsync(AuditoriumBl auditoriumBl)
        {
            var auditoriumData = _mapper.Map<Auditorium>(auditoriumBl);
            using (var ctx = _context())
            {
                ctx.Auditoria.Add(auditoriumData);
                await ctx.SaveChangesAsync();
            }
            auditoriumBl.Id = auditoriumData.Id;
        }

        public async Task AddCinemaAsync(CinemaBlToAdd cinema)
        {
            using (var ctx = _context())
            {
                var cinemaData = _mapper.Map<Cinema>(cinema);

                foreach (var item in cinema.Auditoria)
                {
                    cinemaData.Auditoria.Add(ctx.Auditoria.Add(_mapper.Map<Auditorium>(item)));
                }

                ctx.Cinemas.Add(cinemaData);
                await ctx.SaveChangesAsync();
            }
        }

        public async Task<List<CinemaBl>> GetAllCinemasAsync()
        {
            using (var ctx = _context())
            {
                return (await ctx.Cinemas.Include(c => c.Auditoria).ToListAsync()).Select(c => _mapper.Map<CinemaBl>(c)).ToList();
            }
        }

        public async Task<List<AuditoriumBl>> GetCinemasAuditoriaAsync(int cinemaId)
        {
            using (var ctx = _context())
            {
                var auditoria = (await ctx.Cinemas.Include(c => c.Auditoria).FirstOrDefaultAsync(c => c.Id == cinemaId))?.Auditoria;
                var listToReturn = new List<AuditoriumBl>();
                if (auditoria != null)
                    foreach (var auditorium in auditoria)
                    {
                        listToReturn.Add(_mapper.Map<AuditoriumBl>(auditorium));
                    }

                return listToReturn;
            }
        }

        public async Task UpdateCinemaAsync(int cinemaId, AuditoriumBl auditoriumBl)
        {
            using (var ctx = _context())
            {
                var cinemaData = await ctx.Cinemas.FindAsync(cinemaId);
                cinemaData?.Auditoria.Add(ctx.Auditoria.Add(_mapper.Map<Auditorium>(auditoriumBl)));
                await ctx.SaveChangesAsync();
            }
        }

        public async Task<bool> IsAuditoriumNumberAvailableAsync(int cinemaId, AuditoriumBl auditoriumBl)
        {
            using (var ctx = _context())
            {
                var cinemaData = await ctx.Cinemas.Include(c => c.Auditoria).FirstOrDefaultAsync(c => c.Id == cinemaId);

                if (cinemaData?.Auditoria.Where(a => a.AuditoriumNumber == auditoriumBl.AuditoriumNumber).Count() != 0)
                {
                    return false;
                }

                return true;
            }
        }

        public async Task<CinemaBl> GetCinemaBlAsync(int cinemaId)
        {
            using (var ctx = _context())
            {
                if (await ctx.Cinemas.FindAsync(cinemaId) == null)
                {
                    return null;
                }
                return _mapper.Map<CinemaBl>(await ctx.Cinemas.Include(c => c.Auditoria).FirstOrDefaultAsync(c => c.Id == cinemaId));
            }
        }

        public async Task<AuditoriumBl> GetAuditoriumBlAsync(int auditoriumId)
        {
            using (var ctx = _context())
            {
                return _mapper.Map<AuditoriumBl>(await ctx.Auditoria.FindAsync(auditoriumId));
            }
        }

        public async Task<AuditoriumView> GetAuditoriumViewAsync(int screeningId)
        {
            using (var ctx = _context())
            {
                var screening = await ctx.Screenings.Include(s => s.Auditorium).FirstOrDefaultAsync(s => s.Id == screeningId);
                var auditorium = await ctx.Auditoria.FindAsync(screening.Auditorium.Id);
                var auditoriumView = new AuditoriumView();
                for (int i = 0; i < auditorium.Rows; i++)
                {
                    var charAnumber = (int)'A';
                    var charToAddNumber = charAnumber + i;
                    auditoriumView.Rows.Add((char)charToAddNumber);
                }
                for (int i = 1; i <= auditorium.SeatsInRow; i++)
                {
                    auditoriumView.SeatsInRow.Add(i);
                }
                return auditoriumView;
            }
        }

        public async Task<List<CinemaGeneralInfo>> GetAllCinemasWithGeneralInfoAsync()
        {
            using (var ctx = _context())
            {
                return (await ctx.Cinemas.ToListAsync()).Select(c => _mapper.Map<CinemaGeneralInfo>(c)).ToList();
            }
        }
    }
}
