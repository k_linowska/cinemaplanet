﻿using System.ComponentModel.DataAnnotations;

namespace CinemaPlanet.Business.Models
{
    public class ClientRegistration
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string PasswordHash { get; set; }
    }
}
