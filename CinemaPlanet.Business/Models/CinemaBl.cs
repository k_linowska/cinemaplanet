﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CinemaPlanet.Business.Models
{
    public class CinemaBl
    {
        public CinemaBl()
        {
            AuditoriaId = new List<int>();
            Auditoria = new List<AuditoriumBl>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime OpeningHour { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }

        [IgnoreDataMember]
        public List<AuditoriumBl> Auditoria { get; set; }

        [IgnoreDataMember]
        public List<int> AuditoriaId { get; set; }
    }
}
