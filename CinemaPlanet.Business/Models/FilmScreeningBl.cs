﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace CinemaPlanet.Business.Models
{
    public class FilmScreeningBl
    {
        public FilmScreeningBl()
        {
            ScreeningsId = new List<int>();
            Screenings = new List<ScreeningBl>();
        }

        public int Id { get; set; }

        public FilmBl Film { get; set; }

        [Required]
        public int FilmId { get; set; }

        public List<ScreeningBl> Screenings { get; set; }

        [IgnoreDataMember]
        public List<int> ScreeningsId { get; set; }
    }
}
