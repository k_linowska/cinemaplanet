﻿using System.ComponentModel.DataAnnotations;

namespace CinemaPlanet.Business.Models
{
    public class AuditoriumBl
    {
        public int Id { get; set; }

        [Required]
        public int AuditoriumNumber { get; set; }

        [Required]
        [Range(1, 26)]
        public int Rows { get; set; }

        [Required]
        public int SeatsInRow { get; set; }
    }
}
