﻿using System.Collections.Generic;

namespace CinemaPlanet.Business.Models
{
    public class PagedResults<T>
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public int PagesCount { get; set; }
        public int ResultsCount { get; set; }
        public string NextPageAttributes { get; set; }
        public string PrevPageAttributes { get; set; }
        public IEnumerable<T> Results { get; set; }
    }
}
