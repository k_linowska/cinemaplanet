﻿using System;
using System.Collections.Generic;

namespace CinemaPlanet.Business.Models
{
    public class PurchaseDetails
    {
        public int ScreeningId { get; set; }
        public List<string> Seats { get; set; }
        public DateTime DateTimeOfScreening { get; set; }
        public CinemaBl Cinema { get; set; }
        public FilmBl Film { get; set; }
        public decimal Price { get; set; }
        public int ClientId { get; set; }
        public ClientBl Client { get; set; }
    }
}
