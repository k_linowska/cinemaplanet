﻿namespace CinemaPlanet.Business.Models
{
    public class CinemaGeneralInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
    }
}
