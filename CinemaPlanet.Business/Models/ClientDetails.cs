﻿namespace CinemaPlanet.Business.Models
{
    public class ClientDetails
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }
    }
}
