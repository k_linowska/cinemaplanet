﻿using System;

namespace CinemaPlanet.Business.Models
{
    public class TicketDetails
    {
        public string FilmTitle { get; set; }
        public DateTime ScreeningDateTime { get; set; }
        public DateTime TimeOfPurchase { get; set; }
        public string CinemaName { get; set; }
        public string CinemaAddress { get; set; }
        public string ClientSeat { get; set; }
    }
}
