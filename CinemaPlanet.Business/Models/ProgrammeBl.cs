﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace CinemaPlanet.Business.Models
{
    public class ProgrammeBl
    {
        public ProgrammeBl()
        {
            FilmScreeningsId = new List<int>();
            FilmScreenings = new List<FilmScreeningBl>();
        }

        public int Id { get; set; }
        public CinemaBl Cinema { get; set; }

        [Required]
        public int CinemaId { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime StartDay { get; set; }

        public string Week { get; set; }
        public List<FilmScreeningBl> FilmScreenings { get; set; }

        [IgnoreDataMember]
        public List<int> FilmScreeningsId { get; set; }
    }
}
