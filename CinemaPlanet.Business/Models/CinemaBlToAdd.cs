﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CinemaPlanet.Business.Models
{
    public class CinemaBlToAdd
    {
        public CinemaBlToAdd()
        {
            AuditoriaId = new List<int>();
            Auditoria = new List<AuditoriumBl>();
        }

        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public DateTime OpeningHour { get; set; }

        [Required]
        public string StreetName { get; set; }

        [Required]
        public string StreetNumber { get; set; }

        [Required]
        [RegularExpression(@"^(?<groupOne>[0-9]{2})-(?<groupTwo>[0-9]{3})$")]
        public string Postcode { get; set; }

        [Required]
        public string City { get; set; }

        [Required]
        [RegularExpression(
            @"^\(?(?<internationalPrefix>([0]{2}|\+)?[0-9]{2})?[ ]?(?<nationalAreaCode>\(?[0-9]{2}\)?)[ ]?(?<firstThree>[0-9]{3})\)?[-. ]?(?<secondTwo>[0-9]{2})[-. ]?(?<thirdTwo>[0-9]{2})$|^\(?\+?(?<internationalPrefix>[0-9]{2})?[ ]?\)?(?<firstThree>\+?[0-9]{3})?[ ]?(?<secondThree>[0-9]{3})\)?[-. ]?(?<thirdThree>[0-9]{3})$")]
        public string PhoneNumber { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        public List<AuditoriumBl> Auditoria { get; set; }
        public List<int> AuditoriaId { get; set; }
    }
}
