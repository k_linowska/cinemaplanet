﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CinemaPlanet.Business.Models
{
    public class ClientBl
    {
        public ClientBl()
        {
            Tickets = new List<TicketDetails>();
            TicketsId = new List<int>();
        }

        public int Id { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        public string PasswordHash { get; set; }

        public List<int> TicketsId { get; set; }
        public List<TicketDetails> Tickets { get; set; }
    }
}
