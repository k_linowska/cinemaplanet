﻿using System.ComponentModel.DataAnnotations;

namespace CinemaPlanet.Business.Models
{
    public enum Genre
    {
        Action = 1,
        Adventure,
        Animation,
        Biography,
        Comedy,
        Crime,
        Drama,
        Family,
        Fantasy,
        History,
        Horror,
        Musical,
        Romance,
        SciFi,
        Thriller
    }

    public class FilmBl
    {
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public Genre FilmGenre { get; set; }

        [Required]
        public int RunningTime { get; set; }

        [Required]
        [MaxLength(400)]
        public string Summary { get; set; }

        [Required]
        [Url]
        public string Website { get; set; }
    }
}
