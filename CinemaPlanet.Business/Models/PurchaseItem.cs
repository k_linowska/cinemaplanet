﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CinemaPlanet.Business.Helpers;

namespace CinemaPlanet.Business.Models
{
    public class PurchaseItem
    {
        [Required]
        public int ScreeningId { get; set; }

        [CannotBeEmpty]
        [SeatsValidation]
        public List<string> Seats { get; set; }

        [Required]
        public DateTime DayOfScreening { get; set; }

        public int ClientId { get; set; }
    }
}
