﻿using System;

namespace CinemaPlanet.Business.Models
{
    public class TicketBl
    {
        public int Id { get; set; }
        public ClientBl Client { get; set; }
        public FilmBl Film { get; set; }
        public ScreeningBl Screening { get; set; }
        public string FilmTitle { get; set; }
        public DateTime ScreeningDateTime { get; set; }
        public DateTime TimeOfPurchase { get; set; }
        public CinemaBl Cinema { get; set; }
        public int ClientId { get; set; }
        public int ScreeningId { get; set; }
        public string ClientSeat { get; set; }
    }
}
