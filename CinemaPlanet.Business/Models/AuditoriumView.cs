﻿using System.Collections.Generic;

namespace CinemaPlanet.Business.Models
{
    public class AuditoriumView
    {
        public List<char> Rows { get; set; } = new List<char>();
        public List<int> SeatsInRow { get; set; } = new List<int>();
    }
}
