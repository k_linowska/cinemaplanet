﻿using System.ComponentModel.DataAnnotations;

namespace CinemaPlanet.Business.Models
{
    public class ClientAuthData
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string PasswordHash { get; set; }
    }
}
