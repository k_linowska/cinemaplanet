﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CinemaPlanet.Business.Models
{
    public class ScreeningBl
    {
        public int Id { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime TimeOfScreening { get; set; }

        [Required]
        public int AuditoriumNumber { get; set; }

        public int AuditoriumId { get; set; }

    }
}
