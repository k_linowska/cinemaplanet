﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using CinemaPlanet.Business.Models;
using CinemaPlanet.Business.Services;
using CinemaPlanet.WebApi.Helpers;

namespace CinemaPlanet.WebApi.Controllers
{
    public class CinemaController : ApiController
    {
        private readonly ICinemasService _cinemasService;

        public CinemaController(ICinemasService cinemasService)
        {
            _cinemasService = cinemasService;
        }

        [CheckModelForNull]
        [ValidateModelState]
        public async Task<List<CinemaGeneralInfo>> Get()
        {
            try
            {
                return await _cinemasService.GetAllCinemasWithGeneralInfoAsync();
            }
            catch (Exception)
            {
                return null;
            }
        }

        [CheckModelForNull]
        [ValidateModelState]
        public async Task<IHttpActionResult> Post(CinemaBlToAdd cinema)
        {
            try
            {
                await _cinemasService.AddCinemaAsync(cinema);
                return StatusCode(HttpStatusCode.Created);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [CheckModelForNull]
        [ValidateModelState]
        [Route("api/cinema/{cinemaId}/auditorium")]
        public async Task<IHttpActionResult> Post(AuditoriumBl auditoriumBl, int cinemaId)
        {
            if ((await _cinemasService.GetCinemaBlAsync(cinemaId)) == null)
            {
                return BadRequest($"Cinema with id {cinemaId} does not exist");
            }

            if (!await _cinemasService.IsAuditoriumNumberAvailableAsync(cinemaId, auditoriumBl))
            {
                return BadRequest("Auditorium number already added");
            }

            try
            {
                await _cinemasService.UpdateCinemaAsync(cinemaId, auditoriumBl);
                return StatusCode(HttpStatusCode.Created);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
    }
}
