﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using CinemaPlanet.Business.Models;
using CinemaPlanet.Business.Services;
using CinemaPlanet.WebApi.Helpers;

namespace CinemaPlanet.WebApi.Controllers
{
    public class ClientController : ApiController
    {
        private readonly ITicketsService _ticketsService;
        private readonly IClientsService _clientsService;
        private readonly IPurchaseService _purchaseService;

        public ClientController(ITicketsService ticketsService, IClientsService clientsService, IPurchaseService purchaseService)
        {
            _ticketsService = ticketsService;
            _clientsService = clientsService;
            _purchaseService = purchaseService;
        }

        [CheckModelForNull]
        [ValidateModelState]
        [Route("api/client/register")]
        public async Task<IHttpActionResult> Post(ClientRegistration client)
        {
            try
            {
                if (await _clientsService.IsEmailTaken(client.Email))
                {
                    return BadRequest($"Client with email {client.Email} already added");
                }

                await _clientsService.AddCLient(client);
                return StatusCode(HttpStatusCode.Created);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [CheckModelForNull]
        [ValidateModelState]
        public async Task<ClientDetails> AuthorizeUser(ClientAuthData client)
        {
            return await _clientsService.AuthorizeUser(client);
        }

        [CheckModelForNull]
        [ValidateModelState]
        [Route("api/client/{clientId}/tickets")]
        public async Task<IHttpActionResult> Post(int clientId, PurchaseItem purchase)
        {
            try
            {
                var purchaseDetails = new PurchaseDetails
                {
                    ClientId = clientId,
                    ScreeningId = purchase.ScreeningId,
                    Seats = purchase.Seats
                };

                if (await _purchaseService.IsPurchaseValidAsync(purchaseDetails, purchase.DayOfScreening))
                {
                    await _ticketsService.AddTicketAsync(purchaseDetails);
                    _purchaseService.SendReceipt(purchaseDetails);
                    return StatusCode(HttpStatusCode.Created);
                }

                return BadRequest("Invalid data");

            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [CheckModelForNull]
        [ValidateModelState]
        [Route("api/client/{clientId}/tickets")]
        public async Task<PagedResults<TicketDetails>> Get(int clientId, int pageSize = 10, int page = 1)
        {
            return await _ticketsService.GetTicketsHistoryPage(pageSize, page, clientId);
        }
    }
}
