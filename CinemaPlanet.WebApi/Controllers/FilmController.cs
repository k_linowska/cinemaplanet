﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using CinemaPlanet.Business.Models;
using CinemaPlanet.Business.Services;
using CinemaPlanet.WebApi.Helpers;

namespace CinemaPlanet.WebApi.Controllers
{
    public class FilmController : ApiController
    {
        private readonly IFilmsService _filmsService;

        public FilmController(IFilmsService filmsService)
        {
            _filmsService = filmsService;
        }

        [CheckModelForNull]
        [ValidateModelState]
        public async Task<IHttpActionResult> Post(FilmBl film)
        {
            var url = new Uri(film.Website);

            if (url.Host != "www.filmweb.pl" || url.LocalPath.Equals("/"))
            {
                return BadRequest($"{film.Website} is invalid");
            }

            try
            {
                await _filmsService.AddFilmAsync(film);
                return StatusCode(HttpStatusCode.Created);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
    }
}
