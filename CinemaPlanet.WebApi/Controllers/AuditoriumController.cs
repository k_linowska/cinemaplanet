﻿using CinemaPlanet.Business.Models;
using CinemaPlanet.Business.Services;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace CinemaPlanet.WebApi.Controllers
{
    public class AuditoriumController : ApiController
    {
        private readonly ICinemasService _cinemasService;

        public AuditoriumController(ICinemasService cinemasService)
        {
            _cinemasService = cinemasService;
        }

        [Route("api/auditorium/{screeningId}")]
        public async Task<AuditoriumView> Get(int screeningId)
        {
            try
            {
                return await _cinemasService.GetAuditoriumViewAsync(screeningId);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
