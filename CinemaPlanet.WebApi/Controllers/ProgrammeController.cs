﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using CinemaPlanet.Business.Helpers;
using CinemaPlanet.Business.Models;
using CinemaPlanet.Business.Services;
using CinemaPlanet.WebApi.Helpers;

namespace CinemaPlanet.WebApi.Controllers
{
    public class ProgrammeController : ApiController
    {
        private readonly IProgrammesService _programmesService;
        private readonly ICinemasService _cinemasService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IProgrammeValidator _programmeValidator;

        public ProgrammeController(
            IProgrammesService programmesService,
            ICinemasService cinemasService,
            IDateTimeHelper dateTimeHelper,
            IProgrammeValidator programmeValidator)
        {
            _programmesService = programmesService;
            _cinemasService = cinemasService;
            _dateTimeHelper = dateTimeHelper;
            _programmeValidator = programmeValidator;
        }

        [CheckModelForNull]
        [ValidateModelState]
        public async Task<IHttpActionResult> Get([FromUri]DateTime screeningDay, [FromUri]int cinemaId)
        {
            try
            {
                if ((await _cinemasService.GetCinemaBlAsync(cinemaId)) == null)
                {
                    return BadRequest($"Cinema with id {cinemaId} does not exist");
                }
                var programme = await _programmesService.FindProgrammeAsync(_dateTimeHelper.GetWeek(screeningDay), cinemaId);
                if (programme == null)
                {
                    return BadRequest();
                }
                return Ok(programme);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [CheckModelForNull]
        [ValidateModelState]
        public async Task<IHttpActionResult> Post(ProgrammeBl programmeBl)
        {
            try
            {
                if (!await _programmeValidator.IsProgrammeValid(programmeBl))
                {
                    return BadRequest("Incorrect screening hours");
                }

                await _programmesService.AddProgrammeAsync(programmeBl);
                return StatusCode(HttpStatusCode.Created);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
    }
}
