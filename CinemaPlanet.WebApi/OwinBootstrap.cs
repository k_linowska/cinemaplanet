﻿using System;
using Microsoft.Owin.Hosting;

namespace CinemaPlanet.WebApi
{
    internal class OwinBootstrap
    {
        private readonly IAppSettings _appSettings = new AppSettings();
        private IDisposable _webApi;

        public void Start()
        {
            var baseAddress = _appSettings.WebApiUri;
            _webApi = WebApp.Start<Startup>(baseAddress);
        }

        public void Stop()
        {
            _webApi.Dispose();
        }
    }
}