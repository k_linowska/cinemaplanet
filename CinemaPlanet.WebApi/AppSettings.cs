﻿using System.Configuration;

namespace CinemaPlanet.WebApi
{
    internal interface IAppSettings
    {
        string WebApiUri { get; }
    }

    internal class AppSettings : IAppSettings
    {
        public string WebApiUri => ConfigurationManager.AppSettings["WebApiUri"];
    }
}