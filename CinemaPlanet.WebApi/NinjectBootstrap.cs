﻿using Ninject;
using CinemaPlanet.Business;

namespace CinemaPlanet.WebApi
{
    public class NinjectBootstrap
    {
        public IKernel GetKernel()
        {            
            var kernel = new StandardKernel(new CinemaPlanetBusinessModule());
            return kernel;
        }
    }
}