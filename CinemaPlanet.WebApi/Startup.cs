﻿using System.Web.Http;
using System.Web.Http.Cors;
using CinemaPlanet.WebApi.Helpers;
using Ninject.Web.Common.OwinHost;
using Ninject.Web.WebApi.OwinHost;
using Owin;

namespace CinemaPlanet.WebApi
{
    public class Startup
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            var config = new HttpConfiguration();
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Filters.Add(new CheckModelForNullAttribute());
            config.Filters.Add(new ValidateModelStateAttribute());

            config.EnableCors(new EnableCorsAttribute("*", "*", "*"));

            appBuilder.UseNinjectMiddleware(new NinjectBootstrap().GetKernel).UseNinjectWebApi(config);
        }
    }
}