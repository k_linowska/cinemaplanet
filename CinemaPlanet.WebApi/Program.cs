﻿using System;
using Topshelf;

namespace CinemaPlanet.WebApi
{
    public class Program
    {
        static void Main(string[] args)
        {
            var rc = HostFactory.Run(x =>
            {
                x.Service<OwinBootstrap>(s =>
                {
                    s.ConstructUsing(name => new OwinBootstrap());
                    s.WhenStarted(ob => ob.Start());
                    s.WhenStopped(ob => ob.Stop());
                });
                x.RunAsLocalSystem();

                x.SetDescription("CinemaPlanet server WebApi Service");
                x.SetDisplayName("CinemaPlanetWebApi");
                x.SetServiceName("CinemaPlanetWebApi");
            });

            var exitCode = (int)Convert.ChangeType(rc, rc.GetTypeCode());
            Environment.ExitCode = exitCode;
        }
    }
}
