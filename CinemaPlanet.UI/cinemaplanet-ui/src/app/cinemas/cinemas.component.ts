import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Cinema } from '../models/cinema';

@Component({
  selector: 'app-cinemas',
  templateUrl: './cinemas.component.html',
  styleUrls: ['./cinemas.component.css']
})
export class CinemasComponent implements OnInit {

  public cinemas : Cinema[];
  public date : Date = new Date();

  constructor(private http: HttpClient) { }

  ngOnInit() : void {
    this.getCinemas();
  }

  private getCinemas() : void {
    this.http.get<Cinema[]>('http://localhost:5000/api/cinema')
    .subscribe(
      x => this.cinemas = x,
      () => console.log('Error getting operation history')
    )
  }
}
