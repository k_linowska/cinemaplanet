import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { RegistrationUser } from '../models/registrationUser';
import { Md5 } from 'ts-md5';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public firstName : string;
  public lastName : string;
  public email : string;
  public password : string;
  public confirmPassword : string;

  constructor(
    private http : HttpClient,
    private router : Router,
    private toastr : ToastrService) { }

  ngOnInit() {
  }

  public performRegistration() : void {
    if(this.firstName === undefined || this.lastName === undefined || this.email === undefined
      || this.password === undefined || this.confirmPassword === undefined){
      this.toastr.error('Provide all registration data', 'Failure');
      return;
    }

    if(this.password !== this.confirmPassword) {
      this.toastr.error('Confirm password', 'Failure');
      return;
    }

    let user : RegistrationUser = {
      FirstName: this.firstName,
      LastName: this.lastName,
      Email: this.email,
      PasswordHash: Md5.hashStr(this.password).toString()
    }
    
    this.http.post("http://localhost:5000/api/client/register", user)
      .subscribe(
        () => { 
          this.toastr.success('Registration successful', 'Success'); 
          this.router.navigateByUrl('/login');   
        },     
        () => this.toastr.error('Registration failed', 'Failure')      
      );    
  }

  public registerOnEnter(event) : void {
    if(event.key === 'Enter'){
      this.performRegistration();
    }
  }
}
