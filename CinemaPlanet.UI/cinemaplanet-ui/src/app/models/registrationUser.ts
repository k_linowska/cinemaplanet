export class RegistrationUser {
  public FirstName : string;
  public LastName : string;
  public Email : string;
  public PasswordHash : string;
}