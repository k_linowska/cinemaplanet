export class TicketDetails {
  public FilmTitle : string;
  public ScreeningDateTime : Date;
  public TimeOfPurchase : Date;
  public CinemaName : string;
  public CinemaAddress : string;
  public ClientSeat : string;
}