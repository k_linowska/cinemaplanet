export enum Genre {
  Action = 1,
  Adventure,
  Animation,
  Biography,
  Comedy,
  Crime,
  Drama,
  Family,
  Fantasy,
  History,
  Horror,
  Musical,
  Romance,
  SciFi,
  Thriller
}