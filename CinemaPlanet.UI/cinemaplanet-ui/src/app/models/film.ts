import { Genre } from './genre';

export class Film {
  public Title : string;
  public Summary : string;
  public FilmGenre : Genre;
  public RunningTime : number;
}

