export class Screening {
  public Id : number;
  public TimeOfScreening : Date;
  public AuditoriumNumber : number;
}