export class Cinema {
  public Id : number;
  public Name : string;
  public City : string;
  public Address : string;
}