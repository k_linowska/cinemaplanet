import { Cinema } from './cinema';
import { FilmScreening } from './filmScreening';

export class Programme {
  public Id : number;
  public Week : string;
  public Cinema : Cinema;
  public FilmScreenings : FilmScreening[];
}