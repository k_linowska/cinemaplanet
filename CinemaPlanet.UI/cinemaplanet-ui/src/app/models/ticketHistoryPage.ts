import { TicketDetails } from './ticketDetails';

export class TicketHistoryPage {
  public Page : number;
  public PageSize : number;
  public ResultsCount : number;
  public PagesCount : number;
  public NextPageAttributes : string;
  public PrevPageAttributes : string;
  public Results : TicketDetails[];
}