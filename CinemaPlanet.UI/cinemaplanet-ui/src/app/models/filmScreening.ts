import { Screening } from './screening';
import { Film } from './film';

export class FilmScreening {
  public Film : Film;
  public Screenings : Screening[];
}