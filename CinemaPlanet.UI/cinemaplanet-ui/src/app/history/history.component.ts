import { Component, OnInit } from '@angular/core';
import { TicketHistoryPage } from '../models/ticketHistoryPage';
import { HttpClient } from '@angular/common/http';
import { LocalStorageService } from '../services/localStorageService';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {

  public history : TicketHistoryPage = new TicketHistoryPage();

  constructor(
    private localStorageService: LocalStorageService,
    private http: HttpClient) { }

  ngOnInit() : void {
    this.getHistoryPage();
  }

  public showPrevPage() : void {
    this.getHistoryPage(this.history.PrevPageAttributes);
  }

  public showNextPage() : void {
    this.getHistoryPage(this.history.NextPageAttributes);    
  }

  public goToPage(page : number) : void {
    this.getHistoryPage('?page=' + page + '&pageSize=' + this.history.PageSize);
  }

  public changePageSize(pageSize : number) {
    this.history.PageSize = pageSize;
    this.getHistoryPage();
  }

  public getPageIndexes() : number[] {
    let indexes : number[] = [];
    for(let i = 1; i <= this.history.PagesCount; i++){
      indexes.push(i);
    }
    return indexes;
  }

  private getHistoryPage(urlAttributes : string = '') : void {
    this.http.get<TicketHistoryPage>('http://localhost:5000/api/client/' + this.localStorageService.readCredentials().Id + '/tickets' + urlAttributes)
    .subscribe(
      x => this.history = x,
      () => console.log('Error getting operation history')
    )
  }
}
