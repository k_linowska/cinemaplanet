import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Md5 } from 'ts-md5/dist/md5';
import { LocalStorageService } from '../services/localStorageService';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../models/user';
import { UserDetails } from '../models/userDetails';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public login : string;
  public password : string;

  constructor(
    private http : HttpClient,
    private toastr : ToastrService,
    private route : ActivatedRoute,
    private router : Router,
    private localStorageService : LocalStorageService) { }

  ngOnInit() {
  }

  public performLogin() : void {
    if(this.login === undefined || this.password === undefined) {
      this.toastr.error('Provide login and password', 'Failure');
      return;
    }

    let user : User = {
      Email: this.login,
      PasswordHash: Md5.hashStr(this.password).toString()
    }

    this.http.post<UserDetails>("http://localhost:5000/api/client", user)
      .subscribe(
        x => this.logonCallback(x),
        () => this.toastr.error('Login failed', 'Failure')      
      );
  }

  public isUserLoggedIn() : boolean {
    return this.localStorageService.readCredentials() !== null;
  }

  public performLogout() : void {
    this.localStorageService.saveCredentials(null);
    this.toastr.success('Logout successful', 'Success');
    this.router.navigateByUrl('/');
  }

  private logonCallback(userDetails : UserDetails) : void { 
    debugger;
    if(userDetails !== null) {
      this.toastr.success('Login successful', 'Success');
      this.localStorageService.saveCredentials(userDetails); 
      let returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
      this.router.navigateByUrl(returnUrl);
    } 
    else {
      this.toastr.error('Invalid credentials', 'Failure');
    }     
  }

  public loginOnEnter(event) : void {
    if(event.key === 'Enter'){
      this.performLogin();
    }
  }
}
