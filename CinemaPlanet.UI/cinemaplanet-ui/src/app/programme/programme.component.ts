import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Programme } from '../models/programme';
import { ToastrService } from 'ngx-toastr';
import { FilmScreening } from '../models/filmScreening';
import { Genre } from '../models/genre';

@Component({
  selector: 'app-programme',
  templateUrl: './programme.component.html',
  styleUrls: ['./programme.component.css']
})
export class ProgrammeComponent implements OnInit {

  public programme : Programme;
  public date : Date = new Date();
  public chosenScreeningId : number;
  public screeningDay : string;
  public genre: typeof Genre = Genre;

  constructor(
    private http : HttpClient,
    private toastr : ToastrService,
    private router : Router,
    private route : ActivatedRoute) { }
    
  ngOnInit() {
    let cinemaId = this.route.snapshot.paramMap.get('cinemaId');
    this.screeningDay = this.route.snapshot.paramMap.get('screeningDay');

    this.http.get<Programme>(
      'http://localhost:5000/api/programme/?cinemaId=' + cinemaId + '&screeningDay=' + this.screeningDay)
    .subscribe(
      x => this.programme = x,
      () => {this.toastr.error('Programme not found'); this.router.navigateByUrl('/');}
    );
  }

  public getScreeningsTime(filmScreening: FilmScreening): string { 
    let timeOfScreening = filmScreening.Screenings.map(function (elem) { 
      let dateTime = new Date(elem.TimeOfScreening);
      var options = {
        hour: "2-digit",
        minute: "2-digit"
      }
      return dateTime.toLocaleTimeString("en", options); 
    }).join(", "); 
    return timeOfScreening; 
  }

  public buyTicket() {
    this.router.navigateByUrl('/purchase/'+ this.chosenScreeningId + '/' + this.screeningDay);
  }
}
