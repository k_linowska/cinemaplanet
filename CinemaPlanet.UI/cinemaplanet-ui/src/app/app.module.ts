import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { CinemasComponent } from './cinemas/cinemas.component';
import { HistoryComponent } from './history/history.component';
import { RegisterComponent } from './register/register.component';
import { AuthGuard } from './services/auth.guard';
import { StorageServiceModule } from 'angular-webstorage-service';
import { ToastrModule } from 'ngx-toastr';
import { LocalStorageService } from './services/localStorageService';
import { RegistrationGuard } from './services/registration.guard';
import { ProgrammeComponent } from './programme/programme.component';
import {NgbModule, NgbDateAdapter, NgbDateNativeAdapter } from '@ng-bootstrap/ng-bootstrap';
import { PurchaseComponent } from './purchase/purchase.component';

const appRoutes: Routes = [
  { path: 'cinemas',  component: CinemasComponent,  pathMatch: 'full' },
  { path: 'register', component: RegisterComponent, canActivate: [RegistrationGuard],pathMatch: 'full' },
  { path: 'history',  component: HistoryComponent,  canActivate: [AuthGuard], pathMatch: 'full'},
  { path: 'purchase',  canActivateChild: [AuthGuard], children: [
    { path: '',    component: PurchaseComponent, pathMatch: 'full' },  
  { path: ':screeningId',  canActivateChild: [AuthGuard], children: [
    { path: ':screeningDay',    component: PurchaseComponent, pathMatch: 'full' },    
    ] },    
  ]},
   { path: 'programme', canActivateChild: [], children: [
     { path: '',    component: ProgrammeComponent, pathMatch: 'full' },    
     { path: ':cinemaId', canActivateChild: [], children: [
     { path: ':screeningDay',    component: ProgrammeComponent, pathMatch: 'full' },    
     ] },    
   ]  },
  { path: 'login',    component: LoginComponent,    pathMatch: 'full' },
  { path: '',         component: CinemasComponent,     pathMatch: 'full' },
]

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    CinemasComponent,
    HistoryComponent,
    RegisterComponent,
    ProgrammeComponent,
    PurchaseComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    NgbModule.forRoot(),
    RouterModule,
    StorageServiceModule,
    ToastrModule.forRoot(),
    RouterModule.forRoot(
      appRoutes
    ),
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [
    LocalStorageService,
    {
      provide: NgbDateAdapter,
      useClass: NgbDateNativeAdapter
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
