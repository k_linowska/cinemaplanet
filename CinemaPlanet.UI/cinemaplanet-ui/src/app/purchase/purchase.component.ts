import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { LocalStorageService } from '../services/localStorageService';
import { Purchase } from '../models/purchase';
import { Auditorium } from '../models/auditorium';

@Component({
  selector: 'app-purchase',
  templateUrl: './purchase.component.html',
  styleUrls: ['./purchase.component.css']
})
export class PurchaseComponent implements OnInit {

  public seats : string[] = [];
  public chosenSeat : string;
  public screeningId : number;
  public auditorium : Auditorium;
  public userEmail : string;
  public userId : number;
  public cinemaView : boolean = false;

  constructor(
    private http : HttpClient,
    private toastr : ToastrService,
    private router : Router,
    private localStorageService: LocalStorageService,
    private route : ActivatedRoute) { }

  ngOnInit() {
    this.screeningId = +this.route.snapshot.paramMap.get('screeningId');
    this.userId = this.localStorageService.readCredentials().Id;
    this.userEmail = this.localStorageService.readCredentials().Email;
    this.getAuditorium();
    }

    public makePurchase(){
      if(this.seats.length === 0){
        this.toastr.error('Pick one or more seats');
        return;
      }
  
      let purchase : Purchase = {
        DayOfScreening: this.route.snapshot.paramMap.get('screeningDay'),
        ScreeningId: this.screeningId,
        Seats: this.seats
    }

    this.http.post('http://localhost:5000/api/client/' + this.userId + '/tickets', purchase)
    .subscribe(
      () => { 
        this.toastr.success('Purchase successful', 'Success'); 
        this.router.navigateByUrl('/cinemas');   
      },     
      () => this.toastr.error('Purchase failed', 'Failure')      
    );    
  }

  public addSeat(seat : string) {
    if(seat){
      this.seats.push(seat);
    }
  }

  public setSeat(row : string, seat : number) :string {
    return row + seat;
  }

  public getAuditorium() {
    this.http.get<Auditorium>(
      'http://localhost:5000/api/auditorium/' + this.screeningId)
    .subscribe(
      x => this.auditorium = x,
      () => {
        this.toastr.error('Error');
        this.router.navigateByUrl('/');
      }
    );
  }

  public showCinemaView() {
    this.cinemaView = true;
  }
}
