import { Injectable }       from '@angular/core';
import {
  CanActivate, Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivateChild
}                           from '@angular/router';
import { LocalStorageService } from './localStorageService';
import { User } from '../models/user';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root',
})
export class RegistrationGuard implements CanActivate, CanActivateChild {
  constructor(
    private localStorageService: LocalStorageService, 
    private toastr : ToastrService,
    private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    return this.isUserLoggedIn(state);
  }

  canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    return this.canActivate(route, state);
  }

  private isUserLoggedIn(state: RouterStateSnapshot) : boolean {
    var credentials : User = this.localStorageService.readCredentials();
    if(credentials !== null) {
      this.router.navigate(['/cinemas']), { queryParams: { returnUrl: state.url }};
      this.toastr.error('You are already logged in', 'Failure');
      return false;
    }

    return true;
  }
}