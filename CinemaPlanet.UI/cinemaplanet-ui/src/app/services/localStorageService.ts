import { Inject, Injectable } from '@angular/core';
import { LOCAL_STORAGE, WebStorageService } from 'angular-webstorage-service';
import { User } from '../models/user';
import { UserDetails } from '../models/userDetails';

@Injectable()
export class LocalStorageService {
  private credentialsKey = 'cinemaPlanetUi_credentials';

  constructor(@Inject(LOCAL_STORAGE) private storage: WebStorageService){}

  saveCredentials(credentials : User) : void {
      this.storage.set(this.credentialsKey, credentials);
  }

  readCredentials() : UserDetails {
      var credentials: UserDetails = this.storage.get(this.credentialsKey);
      return credentials;
  }
}