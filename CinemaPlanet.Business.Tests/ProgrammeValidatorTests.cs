﻿using System;
using CinemaPlanet.Business.Services;
using NUnit.Framework;

namespace CinemaPlanet.Business.Tests
{
    [TestFixture]
    public class ProgrammeValidatorTests
    {
        [Test]
        public void DoesScreeningStartsAfterOpeningHour_InvalidInput_ReturnFalse()
        {
            DateTime timeOfScreening = new DateTime(2019, 01, 01, 08, 0, 0);
            string openingHour = "09:00";

            var programmeValidator = new ProgrammeValidator(null, null, null, null, null);

            var result = programmeValidator.DoesScreeningStartsAfterOpeningHour(timeOfScreening, openingHour);

            Assert.IsFalse(result);
        }

        [Test]
        public void DoesScreeningStartsAfterOpeningHour_ValidInput_ReturnTrue()
        {
            DateTime timeOfScreening = new DateTime(2019, 01, 01, 09, 0, 0);
            string openingHour = "09:00";

            var programmeValidator = new ProgrammeValidator(null, null, null, null, null);

            var result = programmeValidator.DoesScreeningStartsAfterOpeningHour(timeOfScreening, openingHour);

            Assert.IsTrue(result);
        }

        [Test]
        public void IsScreeningTimeValid_ScreeningOverlapsOtherScreening_ReturnFalse()
        {
            var screeningTimeToValidate = new DateTime(2019, 01, 01, 10, 30, 0);
            var filmsRuntimeToValidate = 120;
            var screeningTimeToCompare = new DateTime(2019, 01, 01, 10, 0, 0);
            var filmsRuntimeToCompare = 100;

            var programmeValidator = new ProgrammeValidator(null, null, null, null, null);

            var result = programmeValidator.IsScreeningTimeValid(screeningTimeToValidate, filmsRuntimeToValidate, screeningTimeToCompare, filmsRuntimeToCompare);

            Assert.IsFalse(result);
        }

        [Test]
        public void IsScreeningTimeValid_ScreeningOverlapsPreviousScreening_ReturnFalse()
        {
            var screeningTimeToValidate = new DateTime(2019, 01, 01, 12, 0, 0);
            var filmsRuntimeToValidate = 120;
            var screeningTimeToCompare = new DateTime(2019, 01, 01, 11, 0, 0);
            var filmsRuntimeToCompare = 100;

            var programmeValidator = new ProgrammeValidator(null, null, null, null, null);

            var result = programmeValidator.IsScreeningTimeValid(screeningTimeToValidate, filmsRuntimeToValidate, screeningTimeToCompare, filmsRuntimeToCompare);

            Assert.IsFalse(result);
        }

        [Test]
        public void IsScreeningTimeValid_ScreeningOverlapsNextScreening_ReturnFalse()
        {
            var screeningTimeToValidate = new DateTime(2019, 01, 01, 12, 0, 0);
            var filmsRuntimeToValidate = 120;
            var screeningTimeToCompare = new DateTime(2019, 01, 01, 13, 0, 0);
            var filmsRuntimeToCompare = 100;

            var programmeValidator = new ProgrammeValidator(null, null, null, null, null);

            var result = programmeValidator.IsScreeningTimeValid(screeningTimeToValidate, filmsRuntimeToValidate, screeningTimeToCompare, filmsRuntimeToCompare);

            Assert.IsFalse(result);
        }

        [Test]
        public void IsScreeningTimeValid_ScreeningStartsAtBreakTime_ReturnFalse()
        {
            var screeningTimeToValidate = new DateTime(2019, 01, 01, 12, 0, 0);
            var filmsRuntimeToValidate = 120;
            var screeningTimeToCompare = new DateTime(2019, 01, 01, 10, 0, 0);
            var filmsRuntimeToCompare = 120;

            var programmeValidator = new ProgrammeValidator(null, null, null, null, null);

            var result = programmeValidator.IsScreeningTimeValid(screeningTimeToValidate, filmsRuntimeToValidate, screeningTimeToCompare, filmsRuntimeToCompare);

            Assert.IsFalse(result);
        }

        [Test]
        public void IsScreeningTimeValid_ValidInput_ReturnTrue()
        {
            var screeningTimeToValidate = new DateTime(2019, 01, 01, 12, 0, 0);
            var filmsRuntimeToValidate = 120;
            var screeningTimeToCompare = new DateTime(2019, 01, 01, 15, 0, 0);
            var filmsRuntimeToCompare = 100;

            var programmeValidator = new ProgrammeValidator(null, null, null, null, null);

            var result = programmeValidator.IsScreeningTimeValid(screeningTimeToValidate, filmsRuntimeToValidate, screeningTimeToCompare, filmsRuntimeToCompare);

            Assert.IsTrue(result);
        }
    }
}
