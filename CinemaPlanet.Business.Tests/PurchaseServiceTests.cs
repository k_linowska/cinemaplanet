﻿using System;
using System.Collections.Generic;
using CinemaPlanet.Business.Helpers;
using CinemaPlanet.Business.Models;
using CinemaPlanet.Business.Services;
using Moq;
using NUnit.Framework;

namespace CinemaPlanet.Business.Tests
{
    [TestFixture]
    public class PurchaseServiceTests
    {
        [Test]
        public void IsSeatTaken_SeatIsUnavailable_ReturnTrue()
        {
            string chosenSeat = "B12";
            var chosenSeats = new List<string> { "B11", "B12" };

            var purchaseService = new PurchaseService(null, null, null, null, null, null, null);
            var result = purchaseService.IsSeatTaken(chosenSeat, chosenSeats);

            Assert.IsTrue(result);
        }

        [Test]
        public void IsSeatTaken_SeatIsAvailable_ReturnFalse()
        {
            string chosenSeat = "B12";
            var chosenSeats = new List<string> { "B11" };

            var purchaseService = new PurchaseService(null, null, null, null, null, null, null);
            var result = purchaseService.IsSeatTaken(chosenSeat, chosenSeats);

            Assert.IsFalse(result);
        }

        [Test]
        public void IsSeatValidForAuditorium_InvalidRow_ReturnFalse()
        {
            string chosenSeat = "Q12";
            var auditorium = new AuditoriumBl { Rows = 14, SeatsInRow = 20 };

            var purchaseService = new PurchaseService(null, null, null, null, null, null, null);
            var result = purchaseService.IsSeatValidForAuditorium(chosenSeat, auditorium);

            Assert.IsFalse(result);
        }

        [Test]
        public void IsSeatValidForAuditorium_InvalidSeatNumber_ReturnFalse()
        {
            string chosenSeat = "G22";
            var auditorium = new AuditoriumBl { Rows = 14, SeatsInRow = 20 };

            var purchaseService = new PurchaseService(null, null, null, null, null, null, null);
            var result = purchaseService.IsSeatValidForAuditorium(chosenSeat, auditorium);

            Assert.IsFalse(result);
        }

        [Test]
        public void IsSeatValidForAuditorium_ValidInput_ReturnTrue()
        {
            string chosenSeat = "G12";
            var auditorium = new AuditoriumBl { Rows = 14, SeatsInRow = 20 };

            var purchaseService = new PurchaseService(null, null, null, null, null, null, null);
            var result = purchaseService.IsSeatValidForAuditorium(chosenSeat, auditorium);

            Assert.IsTrue(result);
        }

        [Test]
        public void SetPriceWithDiscounts_BothDiscountsApplied_ValidOutput()
        {
            var seatsReserved = 3;
            var dateTimeOfScreening = new DateTime(2019, 05, 05, 10, 0, 0);
            int ticketsBoughtTotal = 7;
            int ticketsBoughtInSameMonthCount = 4;

            var purchaseService = new PurchaseService(null, null, null, null, null, null, null);
            var result = purchaseService.SetPriceWithDiscounts(seatsReserved, dateTimeOfScreening, ticketsBoughtTotal, ticketsBoughtInSameMonthCount);

            Assert.AreEqual(45, result);
        }

        [Test]
        public void SetPriceWithDiscounts_FreeTicketDiscountsApplied_ValidOutput()
        {
            var seatsReserved = 3;
            var dateTimeOfScreening = new DateTime(2019, 05, 05, 10, 0, 0);
            int ticketsBoughtTotal = 7;
            int ticketsBoughtInSameMonthCount = 0;

            var purchaseService = new PurchaseService(null, null, null, null, null, null, null);
            var result = purchaseService.SetPriceWithDiscounts(seatsReserved, dateTimeOfScreening, ticketsBoughtTotal, ticketsBoughtInSameMonthCount);

            Assert.AreEqual(50, result);
        }

        [Test]
        public void SetPriceWithDiscounts_20PercentOffDiscountsApplied_ValidOutput()
        {
            var seatsReserved = 1;
            var dateTimeOfScreening = new DateTime(2019, 05, 05, 10, 0, 0);
            int ticketsBoughtTotal = 7;
            int ticketsBoughtInSameMonthCount = 5;

            var purchaseService = new PurchaseService(null, null, null, null, null, null, null);
            var result = purchaseService.SetPriceWithDiscounts(seatsReserved, dateTimeOfScreening, ticketsBoughtTotal, ticketsBoughtInSameMonthCount);

            Assert.AreEqual(20, result);
        }

        [Test]
        public void SetPriceWithDiscounts_NoDiscountsApplied_ValidOutput()
        {
            var seatsReserved = 2;
            var dateTimeOfScreening = new DateTime(2019, 05, 05, 10, 0, 0);
            int ticketsBoughtTotal = 0;
            int ticketsBoughtInSameMonthCount = 0;

            var purchaseService = new PurchaseService(null, null, null, null, null, null, null);
            var result = purchaseService.SetPriceWithDiscounts(seatsReserved, dateTimeOfScreening, ticketsBoughtTotal, ticketsBoughtInSameMonthCount);

            Assert.AreEqual(50, result);
        }

        [Test]
        public void IsPurchaseValidAsync_ClientIsNotRegistered_ReturnFalse()
        {
            var purchase = new PurchaseDetails
            {
                ClientId = 1
            };
            var screeningDate = new DateTime(2020, 05, 05, 10, 0, 0);
            ClientBl client = null;

            var clientServiceMock = new Mock<IClientsService>();

            clientServiceMock.Setup(x => x.GetCLient(purchase.ClientId)).ReturnsAsync(client);

            var purchaseService = new PurchaseService(null, clientServiceMock.Object, null, null, null, null, null);
            var result = purchaseService.IsPurchaseValidAsync(purchase, screeningDate).Result;

            Assert.IsFalse(result);
        }

        [Test]
        public void IsPurchaseValidAsync_ScreeningHasAlreadyTakenPlace_ReturnFalse()
        {
            var purchase = new PurchaseDetails
            {
                ClientId = 1
            };
            var screeningDate = new DateTime(2020, 01, 01, 10, 0, 0);
            var client = new ClientBl { Id = 1 };

            var clientServiceMock = new Mock<IClientsService>();
            var programmesServiceMock = new Mock<IProgrammesService>();

            clientServiceMock.Setup(x => x.GetCLient(purchase.ClientId)).ReturnsAsync(client);

            var purchaseService = new PurchaseService(null, clientServiceMock.Object, null, null, null, programmesServiceMock.Object, null);
            var result = purchaseService.IsPurchaseValidAsync(purchase, screeningDate).Result;

            Assert.IsFalse(result);
        }

        [Test]
        public void IsPurchaseValidAsync_ScreeningDoesNotExist_ReturnFalse()
        {
            var purchase = new PurchaseDetails
            {
                ClientId = 1,
                ScreeningId = 1
            };
            var screeningDate = new DateTime(2020, 05, 05, 10, 0, 0);
            var client = new ClientBl { Id = 1 };
            ScreeningBl screening = null;

            var clientServiceMock = new Mock<IClientsService>();
            var programmesServiceMock = new Mock<IProgrammesService>();

            clientServiceMock.Setup(x => x.GetCLient(purchase.ClientId)).ReturnsAsync(client);
            programmesServiceMock.Setup(x => x.GetScreeningBlAsync(purchase.ScreeningId)).ReturnsAsync(screening);

            var purchaseService = new PurchaseService(null, clientServiceMock.Object, null, null, null, programmesServiceMock.Object, null);
            var result = purchaseService.IsPurchaseValidAsync(purchase, screeningDate).Result;

            Assert.IsFalse(result);
        }

        [Test]
        public void IsPurchaseValidAsync_ScreeningDoesNotMatchScreeningDate_ReturnFalse()
        {
            var purchase = new PurchaseDetails
            {
                ClientId = 1,
                ScreeningId = 1
            };
            var screeningDate = new DateTime(2020, 05, 05, 10, 0, 0);
            var client = new ClientBl { Id = 1 };
            var screening = new ScreeningBl { Id = 1, TimeOfScreening = new DateTime(2020, 04, 05, 10, 0, 0) };
            var filmScreening = new FilmScreeningBl { ScreeningsId = new List<int> { 1 } };
            var programme = new ProgrammeBl
            {
                FilmScreenings = new List<FilmScreeningBl> { filmScreening },
                StartDay = new DateTime(2020, 04, 05, 10, 0, 0)
            };

            var clientServiceMock = new Mock<IClientsService>();
            var programmesServiceMock = new Mock<IProgrammesService>();
            var dateTimeHelperMock = new Mock<IDateTimeHelper>();

            clientServiceMock.Setup(x => x.GetCLient(purchase.ClientId)).ReturnsAsync(client);
            programmesServiceMock.Setup(x => x.GetScreeningBlAsync(purchase.ScreeningId)).ReturnsAsync(screening);
            programmesServiceMock.Setup(x => x.FindProgrammeAsync(purchase.ScreeningId)).ReturnsAsync(programme);

            var purchaseService = new PurchaseService(null, clientServiceMock.Object, null, null, dateTimeHelperMock.Object, programmesServiceMock.Object, null);
            var result = purchaseService.IsPurchaseValidAsync(purchase, screeningDate).Result;

            Assert.IsFalse(result);
        }

        [Test]
        public void IsPurchaseValidAsync_SeatIsNotValid_ReturnFalse()
        {
            var purchase = new PurchaseDetails
            {
                ClientId = 1,
                ScreeningId = 1,
                Seats = new List<string> { "O14" }
            };
            var screeningDate = new DateTime(2020, 05, 05, 10, 0, 0);
            var auditorium = new AuditoriumBl { Rows = 13, SeatsInRow = 18 };
            var client = new ClientBl { Id = 1 };
            var screening = new ScreeningBl { Id = 1, AuditoriumId = 1, TimeOfScreening = new DateTime(2020, 04, 05, 10, 0, 0) };
            var filmScreening = new FilmScreeningBl { ScreeningsId = new List<int> { 1 } };
            var programme = new ProgrammeBl
            {
                FilmScreenings = new List<FilmScreeningBl> { filmScreening },
                StartDay = new DateTime(2020, 05, 05, 10, 0, 0)
            };

            var clientServiceMock = new Mock<IClientsService>();
            var programmesServiceMock = new Mock<IProgrammesService>();
            var dateTimeHelperMock = new Mock<IDateTimeHelper>();
            var cinemasServiceMock = new Mock<ICinemasService>();

            clientServiceMock.Setup(x => x.GetCLient(purchase.ClientId)).ReturnsAsync(client);
            programmesServiceMock.Setup(x => x.GetScreeningBlAsync(purchase.ScreeningId)).ReturnsAsync(screening);
            programmesServiceMock.Setup(x => x.FindProgrammeAsync(purchase.ScreeningId)).ReturnsAsync(programme);
            cinemasServiceMock.Setup(x => x.GetAuditoriumBlAsync(screening.AuditoriumId)).ReturnsAsync(auditorium);

            var purchaseService = new PurchaseService(
                null, clientServiceMock.Object, null, null, dateTimeHelperMock.Object, programmesServiceMock.Object, cinemasServiceMock.Object);
            var result = purchaseService.IsPurchaseValidAsync(purchase, screeningDate).Result;

            Assert.IsFalse(result);
        }

        [Test]
        public void IsPurchaseValidAsync_SeatIsNotAvailable_ReturnFalse()
        {
            var purchase = new PurchaseDetails
            {
                ClientId = 1,
                ScreeningId = 1,
                Seats = new List<string> { "D14" }
            };
            var screeningDate = new DateTime(2020, 05, 05, 10, 0, 0);
            var auditorium = new AuditoriumBl { Rows = 13, SeatsInRow = 18 };
            var client = new ClientBl { Id = 1 };
            var screening = new ScreeningBl { Id = 1, AuditoriumId = 1, TimeOfScreening = new DateTime(2020, 04, 05, 10, 0, 0) };
            var filmScreening = new FilmScreeningBl { ScreeningsId = new List<int> { 1 } };
            var programme = new ProgrammeBl
            {
                FilmScreenings = new List<FilmScreeningBl> { filmScreening },
                StartDay = new DateTime(2020, 05, 05, 10, 0, 0)
            };
            var dateAndTimeOfScreening = new DateTime(2020, 05, 05, 10, 0, 0);

            var clientServiceMock = new Mock<IClientsService>();
            var programmesServiceMock = new Mock<IProgrammesService>();
            var ticketsServiceMock = new Mock<ITicketsService>();
            var dateTimeHelperMock = new Mock<IDateTimeHelper>();
            var cinemasServiceMock = new Mock<ICinemasService>();

            clientServiceMock.Setup(x => x.GetCLient(purchase.ClientId)).ReturnsAsync(client);
            dateTimeHelperMock.Setup(x => x.CombineDates(screeningDate, screening.TimeOfScreening))
                .Returns(dateAndTimeOfScreening);
            programmesServiceMock.Setup(x => x.GetScreeningBlAsync(purchase.ScreeningId)).ReturnsAsync(screening);
            programmesServiceMock.Setup(x => x.FindProgrammeAsync(purchase.ScreeningId)).ReturnsAsync(programme);
            cinemasServiceMock.Setup(x => x.GetAuditoriumBlAsync(screening.AuditoriumId)).ReturnsAsync(auditorium);
            ticketsServiceMock
                .Setup(x => x.GetUnavailableSeatsAsync(purchase.ScreeningId, dateAndTimeOfScreening))
                .ReturnsAsync(new List<string> { "D14" });

            var purchaseService = new PurchaseService(
                null, clientServiceMock.Object, null, ticketsServiceMock.Object, dateTimeHelperMock.Object, programmesServiceMock.Object, cinemasServiceMock.Object);
            var result = purchaseService.IsPurchaseValidAsync(purchase, screeningDate).Result;

            Assert.IsFalse(result);
        }

        [Test]
        public void IsPurchaseValidAsync_ValidInput_ReturnTrue()
        {
            var purchase = new PurchaseDetails
            {
                ClientId = 1,
                ScreeningId = 1,
                Seats = new List<string> { "D14" }
            };
            var screeningDate = new DateTime(2020, 05, 06, 09, 0, 0);
            var auditorium = new AuditoriumBl { Rows = 13, SeatsInRow = 18 };
            var film = new FilmBl { Title = "FilmTitle" };
            var client = new ClientBl { Id = 1 };
            var screening = new ScreeningBl { Id = 1, AuditoriumId = 1, TimeOfScreening = new DateTime(2020, 04, 05, 10, 0, 0) };
            var filmScreening = new FilmScreeningBl { ScreeningsId = new List<int> { 1 } };
            var programme = new ProgrammeBl
            {
                FilmScreenings = new List<FilmScreeningBl> { filmScreening },
                StartDay = new DateTime(2020, 05, 05, 10, 0, 0),
                Cinema = new CinemaBl { Id = 1 }
            };
            var dateAndTimeOfScreening = new DateTime(2020, 05, 06, 10, 0, 0);

            var clientServiceMock = new Mock<IClientsService>();
            var filmsServiceMock = new Mock<IFilmsService>();
            var programmesServiceMock = new Mock<IProgrammesService>();
            var ticketsServiceMock = new Mock<ITicketsService>();
            var dateTimeHelperMock = new Mock<IDateTimeHelper>();
            var cinemasServiceMock = new Mock<ICinemasService>();

            clientServiceMock.Setup(x => x.GetCLient(purchase.ClientId)).ReturnsAsync(client);
            filmsServiceMock.Setup(x => x.FindFilmAsync(purchase.ScreeningId)).ReturnsAsync(film);
            programmesServiceMock.Setup(x => x.GetScreeningBlAsync(purchase.ScreeningId)).ReturnsAsync(screening);
            programmesServiceMock.Setup(x => x.FindProgrammeAsync(purchase.ScreeningId)).ReturnsAsync(programme);
            dateTimeHelperMock.Setup(x => x.CombineDates(screeningDate, screening.TimeOfScreening)).Returns(dateAndTimeOfScreening);
            ticketsServiceMock
                .Setup(x => x.TicketsBoughtByUserAsync(purchase.ClientId))
                .ReturnsAsync(new List<TicketBl>());
            ticketsServiceMock
                .Setup(x => x.GetUnavailableSeatsAsync(purchase.ScreeningId, dateAndTimeOfScreening))
                .ReturnsAsync(new List<string> { "D15" });
            ticketsServiceMock.Setup(x => x.TicketsBoughtInSameMonth(new List<TicketBl>())).Returns(0);
            cinemasServiceMock.Setup(x => x.GetAuditoriumBlAsync(screening.AuditoriumId)).ReturnsAsync(auditorium);

            var purchaseService = new PurchaseService(
                null, clientServiceMock.Object, filmsServiceMock.Object, ticketsServiceMock.Object, dateTimeHelperMock.Object, programmesServiceMock.Object, cinemasServiceMock.Object);
            var result = purchaseService.IsPurchaseValidAsync(purchase, screeningDate).Result;

            Assert.IsTrue(result);
        }
    }
}
