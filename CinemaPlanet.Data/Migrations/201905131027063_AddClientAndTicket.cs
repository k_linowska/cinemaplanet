namespace CinemaPlanet.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddClientAndTicket : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Auditoriums", "Cinema_Id", "dbo.Cinemas");
            DropForeignKey("dbo.Screenings", "FilmScreening_Id", "dbo.FilmScreenings");
            DropForeignKey("dbo.FilmScreenings", "Programme_Id", "dbo.Programmes");
            DropIndex("dbo.Auditoriums", new[] { "Cinema_Id" });
            DropIndex("dbo.FilmScreenings", new[] { "Programme_Id" });
            DropIndex("dbo.Screenings", new[] { "FilmScreening_Id" });
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Tickets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ScreeningDateTime = c.DateTime(nullable: false),
                        ClientSeat = c.String(),
                        TimeOfPurchase = c.DateTime(nullable: false),
                        Client_Id = c.Int(nullable: false),
                        Screening_Id = c.Int(nullable: false),
                        Cinema_Id = c.Int(),
                        Film_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cinemas", t => t.Cinema_Id)
                .ForeignKey("dbo.Clients", t => t.Client_Id, cascadeDelete: true)
                .ForeignKey("dbo.Films", t => t.Film_Id)
                .ForeignKey("dbo.Screenings", t => t.Screening_Id, cascadeDelete: true)
                .Index(t => t.Client_Id)
                .Index(t => t.Screening_Id)
                .Index(t => t.Cinema_Id)
                .Index(t => t.Film_Id);
            
            AlterColumn("dbo.Auditoriums", "Cinema_Id", c => c.Int(nullable: false));
            AlterColumn("dbo.FilmScreenings", "Programme_Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Screenings", "TimeOfScreening", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Screenings", "FilmScreening_Id", c => c.Int(nullable: false));
            CreateIndex("dbo.Auditoriums", "Cinema_Id");
            CreateIndex("dbo.Screenings", "FilmScreening_Id");
            CreateIndex("dbo.FilmScreenings", "Programme_Id");
            AddForeignKey("dbo.Auditoriums", "Cinema_Id", "dbo.Cinemas", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Screenings", "FilmScreening_Id", "dbo.FilmScreenings", "Id", cascadeDelete: true);
            AddForeignKey("dbo.FilmScreenings", "Programme_Id", "dbo.Programmes", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FilmScreenings", "Programme_Id", "dbo.Programmes");
            DropForeignKey("dbo.Screenings", "FilmScreening_Id", "dbo.FilmScreenings");
            DropForeignKey("dbo.Auditoriums", "Cinema_Id", "dbo.Cinemas");
            DropForeignKey("dbo.Tickets", "Screening_Id", "dbo.Screenings");
            DropForeignKey("dbo.Tickets", "Film_Id", "dbo.Films");
            DropForeignKey("dbo.Tickets", "Client_Id", "dbo.Clients");
            DropForeignKey("dbo.Tickets", "Cinema_Id", "dbo.Cinemas");
            DropIndex("dbo.FilmScreenings", new[] { "Programme_Id" });
            DropIndex("dbo.Screenings", new[] { "FilmScreening_Id" });
            DropIndex("dbo.Tickets", new[] { "Film_Id" });
            DropIndex("dbo.Tickets", new[] { "Cinema_Id" });
            DropIndex("dbo.Tickets", new[] { "Screening_Id" });
            DropIndex("dbo.Tickets", new[] { "Client_Id" });
            DropIndex("dbo.Auditoriums", new[] { "Cinema_Id" });
            AlterColumn("dbo.Screenings", "FilmScreening_Id", c => c.Int());
            AlterColumn("dbo.Screenings", "TimeOfScreening", c => c.String());
            AlterColumn("dbo.FilmScreenings", "Programme_Id", c => c.Int());
            AlterColumn("dbo.Auditoriums", "Cinema_Id", c => c.Int());
            DropTable("dbo.Tickets");
            DropTable("dbo.Clients");
            CreateIndex("dbo.Screenings", "FilmScreening_Id");
            CreateIndex("dbo.FilmScreenings", "Programme_Id");
            CreateIndex("dbo.Auditoriums", "Cinema_Id");
            AddForeignKey("dbo.FilmScreenings", "Programme_Id", "dbo.Programmes", "Id");
            AddForeignKey("dbo.Screenings", "FilmScreening_Id", "dbo.FilmScreenings", "Id");
            AddForeignKey("dbo.Auditoriums", "Cinema_Id", "dbo.Cinemas", "Id");
        }
    }
}
