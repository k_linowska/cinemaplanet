namespace CinemaPlanet.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Auditoriums",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AuditoriumNumber = c.Int(nullable: false),
                        Rows = c.Int(nullable: false),
                        SeatsInRow = c.Int(nullable: false),
                        Cinema_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cinemas", t => t.Cinema_Id)
                .Index(t => t.Cinema_Id);
            
            CreateTable(
                "dbo.Cinemas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        OpeningHour = c.String(),
                        Address = c.String(),
                        PhoneNumber = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Films",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        FilmGenre = c.Int(nullable: false),
                        RunningTime = c.Int(nullable: false),
                        Summary = c.String(),
                        Website = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FilmScreenings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Film_Id = c.Int(),
                        Programme_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Films", t => t.Film_Id)
                .ForeignKey("dbo.Programmes", t => t.Programme_Id)
                .Index(t => t.Film_Id)
                .Index(t => t.Programme_Id);
            
            CreateTable(
                "dbo.Screenings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TimeOfScreening = c.String(),
                        Auditorium_Id = c.Int(),
                        FilmScreening_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Auditoriums", t => t.Auditorium_Id)
                .ForeignKey("dbo.FilmScreenings", t => t.FilmScreening_Id)
                .Index(t => t.Auditorium_Id)
                .Index(t => t.FilmScreening_Id);
            
            CreateTable(
                "dbo.Programmes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Week = c.String(),
                        Cinema_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cinemas", t => t.Cinema_Id)
                .Index(t => t.Cinema_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FilmScreenings", "Programme_Id", "dbo.Programmes");
            DropForeignKey("dbo.Programmes", "Cinema_Id", "dbo.Cinemas");
            DropForeignKey("dbo.Screenings", "FilmScreening_Id", "dbo.FilmScreenings");
            DropForeignKey("dbo.Screenings", "Auditorium_Id", "dbo.Auditoriums");
            DropForeignKey("dbo.FilmScreenings", "Film_Id", "dbo.Films");
            DropForeignKey("dbo.Auditoriums", "Cinema_Id", "dbo.Cinemas");
            DropIndex("dbo.Programmes", new[] { "Cinema_Id" });
            DropIndex("dbo.Screenings", new[] { "FilmScreening_Id" });
            DropIndex("dbo.Screenings", new[] { "Auditorium_Id" });
            DropIndex("dbo.FilmScreenings", new[] { "Programme_Id" });
            DropIndex("dbo.FilmScreenings", new[] { "Film_Id" });
            DropIndex("dbo.Auditoriums", new[] { "Cinema_Id" });
            DropTable("dbo.Programmes");
            DropTable("dbo.Screenings");
            DropTable("dbo.FilmScreenings");
            DropTable("dbo.Films");
            DropTable("dbo.Cinemas");
            DropTable("dbo.Auditoriums");
        }
    }
}
