using EfEnumToLookup.LookupGenerator;

namespace CinemaPlanet.Data.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<CinemaPlanet.Data.CinemaPlanetContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "CinemaPlanet.Data.CinemaPlanetContext";
        }

        protected override void Seed(CinemaPlanet.Data.CinemaPlanetContext context)
        {
            //  This method will be called after migrating to the latest version.
            var enumToLookUp = new EnumToLookup();
            enumToLookUp.Apply(context);
            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
