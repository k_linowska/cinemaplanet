﻿using CinemaPlanet.Data.Models;
using System;
using System.Data.Entity;
using System.Threading.Tasks;

namespace CinemaPlanet.Data
{
    public interface ICinemaPlanetContext : IDisposable
    {
        DbSet<Auditorium> Auditoria { get; set; }
        DbSet<Cinema> Cinemas { get; set; }
        DbSet<Film> Films { get; set; }
        DbSet<FilmScreening> FilmScreenings { get; set; }
        DbSet<Programme> Programmes { get; set; }
        DbSet<Screening> Screenings { get; set; }
        DbSet<Client> Clients { get; set; }
        DbSet<Ticket> Tickets { get; set; }

        Task<int> SaveChangesAsync();
    }

    public class CinemaPlanetContext : DbContext, ICinemaPlanetContext
    {
        public CinemaPlanetContext() : base("CinemaPlanetDbConnectionString")
        { }

        public DbSet<Cinema> Cinemas { get; set; }
        public DbSet<Auditorium> Auditoria { get; set; }
        public DbSet<Film> Films { get; set; }
        public DbSet<FilmScreening> FilmScreenings { get; set; }
        public DbSet<Screening> Screenings { get; set; }
        public DbSet<Programme> Programmes { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
    }
}
