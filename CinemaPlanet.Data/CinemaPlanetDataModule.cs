﻿using Ninject.Modules;

namespace CinemaPlanet.Data
{
    public class CinemaPlanetDataModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<ICinemaPlanetContext>().ToMethod(x => new CinemaPlanetContext());
        }
    }
}
