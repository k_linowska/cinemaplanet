﻿using System.Collections.Generic;

namespace CinemaPlanet.Data.Models
{
    public class Programme
    {
        public Programme()
        {
            FilmScreenings = new List<FilmScreening>();
        }

        public int Id { get; set; }
        public Cinema Cinema { get; set; }
        public string Week { get; set; }
        public List<FilmScreening> FilmScreenings { get; set; }
    }
}
