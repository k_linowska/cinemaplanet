﻿using System.Collections.Generic;

namespace CinemaPlanet.Data.Models
{
    public class Client
    {
        public Client()
        {
            Tickets = new List<Ticket>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public List<Ticket> Tickets { get; set; }
    }
}
