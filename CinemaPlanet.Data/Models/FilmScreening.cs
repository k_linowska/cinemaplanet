﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CinemaPlanet.Data.Models
{
    public class FilmScreening
    {
        public FilmScreening()
        {
            Screenings = new List<Screening>();
        }

        public int Id { get; set; }
        public Film Film { get; set; }
        public List<Screening> Screenings { get; set; }

        public int Programme_Id { get; set; }
        [ForeignKey("Programme_Id")]
        public virtual Programme Programme { get; set; }
    }
}