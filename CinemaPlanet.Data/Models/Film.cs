﻿namespace CinemaPlanet.Data.Models
{
    public enum Genre
    {
        Action = 1,
        Adventure,
        Animation,
        Biography,
        Comedy,
        Crime,
        Drama,
        Family,
        Fantasy,
        History,
        Horror,
        Musical,
        Romance,
        SciFi,
        Thriller
    }

    public class Film
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public Genre FilmGenre { get; set; }
        public int RunningTime { get; set; }
        public string Summary { get; set; }
        public string Website { get; set; }
    }
}
