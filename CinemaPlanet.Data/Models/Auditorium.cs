﻿using System.ComponentModel.DataAnnotations.Schema;

namespace CinemaPlanet.Data.Models
{
    public class Auditorium
    {
        public int Id { get; set; }
        public int AuditoriumNumber { get; set; }
        public int Rows { get; set; }
        public int SeatsInRow { get; set; }

        public int Cinema_Id { get; set; }
        [ForeignKey("Cinema_Id")]
        public virtual Cinema Cinema { get; set; }

    }
}