﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace CinemaPlanet.Data.Models
{
    public class Ticket
    {
        public int Id { get; set; }
        public Film Film { get; set; }
        public DateTime ScreeningDateTime { get; set; }
        public Cinema Cinema { get; set; }
        public string ClientSeat { get; set; }
        public DateTime TimeOfPurchase { get; set; }

        public int Client_Id { get; set; }
        [ForeignKey("Client_Id")]
        public virtual Client Client { get; set; }

        public int Screening_Id { get; set; }
        [ForeignKey("Screening_Id")]
        public virtual Screening Screening { get; set; }
    }
}
