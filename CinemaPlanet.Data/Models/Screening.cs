﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace CinemaPlanet.Data.Models
{
    public class Screening
    {
        public int Id { get; set; }
        public DateTime TimeOfScreening { get; set; }
        public Auditorium Auditorium { get; set; }

        public int FilmScreening_Id { get; set; }
        [ForeignKey("FilmScreening_Id")]
        public virtual FilmScreening FilmScreening { get; set; }
    }
}