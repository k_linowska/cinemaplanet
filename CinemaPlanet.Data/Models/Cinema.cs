﻿using System.Collections.Generic;

namespace CinemaPlanet.Data.Models
{
    public class Cinema
    {
        public Cinema()
        {
            Auditoria = new List<Auditorium>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string OpeningHour { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public List<Auditorium> Auditoria { get; set; }
    }
}
